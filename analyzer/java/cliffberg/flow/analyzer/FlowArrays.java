package cliffberg.flow.analyzer;

import static cliffberg.flow.parser.FlowParser.*;
import static cliffberg.flow.analyzer.ParseTreeUtilities.*;

import cliffberg.symboltable.CompilerState;
import cliffberg.symboltable.TypeDescriptor;
import cliffberg.symboltable.ExprAnnotation;

import org.antlr.v4.runtime.ParserRuleContext;

import java.util.Arrays;
import java.util.List;
import java.util.ArrayList;

/**
 Ref: the Analyzer Design Doc,
 *	https://docs.google.com/document/d/1Lh4bCMkbofyIIs2KhGDXp8yHaVo3RsnXB6Ek2yKL0lM/
 *
 Any new semantic errors detected result in a line position exception. That exception should be
 caught by the caller. Null type descriptors that are encountered are assumed to be the result
 of semantic errors already logged, so when such nulls are found, we merely return.
 */
public class FlowArrays<Type, Node, Start, TId extends Node> {

	private CompilerState<Type, Node, Start, TId> state;

	public FlowArrays(CompilerState<Type, Node, Start, TId> state) {
		this.state = state;
	}

	/** Algorithm AR-1.2:
	Ref: section "Type Analysis of Array Literals" of the Analyzer Design Doc.
	*/
	public void analyzeArrayLiteral(Array_literalContext arrayLiteral, Integer[] range, int dim) {
		range[subOne(dim)] = reconcileRanges(range[subOne(dim)], arrayLiteral.expr_seq().size());
		// Recurse through the literal’s elements:
		// array_literal : Tl_sqbr expr_seq? Tr_sqbr ;
		for (ExprContext eltExpr : arrayLiteral.expr_seq()) { // each element of the arrayLiteral

			if (exprIsArrayLiteral(eltExpr)) // the element is an array literal
				analyzeArrayLiteral(element, range, dim+1);
			else {
				// Obtain the element’s type - might be a multi-dimensional array
				TypeDescriptor eltTypeDesc = this.state.getTypeDesc(eltExpr);
				if (eltTypeDesc == null) return; // cannot analylze

				getArrayTypeDescRanges(arrayElement, range, dim, eltTypeDesc);
			}
		}
	}

	/**
	 Adjust for the fact that Java arrays begin at location 0, whereas in mathematics the
	 convention is to begin at 1.
	 */
	private int subOne(int index) {
		return index-1;
	}

	/**
	 If the specified expression reduces to an array_literal, return true.
	 */
	private boolean exprIsArrayLiteral(ExprContext expr) {
		return (getArrayLiteral(expr) != null);
	}

	/**
	 If the specified expression reduces to an array_literal, return the array_literal; otherwise
	 return null.
	 */
	private Array_literalContext getArrayLiteral(ExprContext expr) {

		Array_literalContext arrayLit = null;
		try {
			arrayLit =
			getOnlyChild(getOnlyChild(getOnlyChild(getOnlyChild(getOnlyChild(getOnlyChild(
			getOnlyChild(getOnlyChild(getOnlyChild(
				expr, Logic_exprContext), Comp_exprContext), Num_exprContext),
				Mult_exprContext), Exp_exprContext), Unop_exprContext), valueContext),
				LiteralContext), Array_literalContext);
		} catch (Exception ex) { // ignore
		}

		return arrayLit;
	}

	private void getArrayTypeDescRanges(Node arrayExpr, Integer[] range, int dim, TypeDescriptor typeDesc) {
		if (typeDesc instanceof ArrayTypeDescriptor) {
			if (typeDesc.size != null)
				range[subOne(dim)] = reconcileRanges(arrayExpr, dim, range[subOne(dim)], typeDesc.size);
			// Recurse through the array expr element types:
			getArrayTypeDescRanges(arrayExpr, range, dim+1, typeDesc.elementTypeDesc);
		} else // a single element may populate an entire range
			reconcileRanges(arrayExpr, dim, range[subOne(dim)], 1);
	}

	/** Algorithm AR-1.3:

	 */
	public void updateArrayLiteralSizes(Array_literalContext arLit, Integer range, int dim) {
		typeDesc = this.state.getTypeDesc(arLit);
		typeDesc.size = range[subOne(dim)];  // side effect: type descriptor is updated
		for (ExprContext eltExpr : arLit.expr_seq()) { // recurse through each literal element
			Array_literalContext childArrayLit = getArrayLiteral(eltExpr);
			if (childArrayLit != null) // element is an array literal
				updateArrayLiteralSizes(childArrayLit, range, dim+1);
		}
	}

	private int reconcileRanges(Node arrayExpr, int dim, int range, int noOfElements) {
		if (range = null) return noOfElements;
		if (range = noOfElements) return range;
		if ((range == 1) || (noOfElements == 1)) return Max(range, noOfElements);
		this.state.throwLinePosExc(arrayExpr, "Inconsistent ranges for dimension " + dim + ": " + range + ", " + noOfElements);
	}

	/** Algorithm AR-2: Determine ranges of array definition: For an array definition,
	build an array of the declared ranges, containing either the provided range (an integer)
	or null (if none provided):
	Ref: section "Array Declarations" of the Analyzer Design Doc.
	*/
	public Long[] getArrayDefRanges(List<Array_def_specContext> arrayDefSpecs) {
		// Create a zero length, expandable array, array of declared ranges:
		ArrayList<Long> declaredRanges = new ArrayList<Long>();

		// Iterate over each array_def_spec - that is, each bracketed range (dimension):
		for (Array_def_specContext arrayDefSpec : arrayDefSpecs) {
			ExprContext rangeExpr = arrayDefSpec.expr();
			if (rangeExpr != null) { // there is a range expression
				// Append the value of the range expression to the array of declared ranges:
				Object value = this.state.getVal(rangeExpr).getValue();
				if (value == null) throwLinePosExp(rangeExpr, "Range not statically determinable");
				if (! (value instanceof FlowValue)) throwLinePosExc(rangeExpr,
					"Flow compatible value not found for range expression");
				FlowValue flowValue = (FlowValue)value;
				if (! (flowValue.valueIsIntegral() && flowValue.isPositiveNumber())) throwLinePosExc(
					rangeExpr, "Range (if present) must be a positive integral expression");
				FlowUnsignedLongIntValue flowUnsLongIntValue;
				try {
					flowUnsLongIntValue = (FlowUnsignedLongIntValue)(convertTo(FlowUnsignedLongInt));
				} catch (Exception ex) {
					throwLinePosExc(rangeExpr, "Unexpected conversion error: " + ex.getMessage());
				}

				declaredRanges.add(flowUnsLongIntValue.value);
			}
			else {
				declaredRanges.add(null); // Append null: null declared ranges are considered to be dynamic.
			}
		}
		return declaredRanges;
	}

	/** Algorithm AR-3: Obtain the ranges of an array type expression: This algorithm assumes
	that the type analysis of array valued expressions that was presented earlier has been
	performed. The intention is to assemble an array of the range of each dimension of the
	source expression.
	Ref: section "Array Value Assignment" of the Analyzer Design Doc.
	*/
	public Long[] getArrayExprRanges(ExprContext expr) {
		// Create a zero length, expandable array, array of expression ranges:
		ArrayList<Long> exprRanges = new ArrayList<Long>();

		TypeDescriptor typeDesc = this.state.getTypeDesc(expr);
		for (;;) {
			if (! (typeDesc instanceof ArrayTypeDescriptor)) // type spec is not an array
				return exprRanges;

			ArrayTypeDescriptor arrayTypeDesc = (ArrayTypeDescriptor)typeDesc;
			if (arrayTypeDesc.size != null) { // array type spec has a size set)
				// Append the size to the array of expression ranges:
				exprRanges.add(arrayTypeDesc.size);
			} else {
				// Append null to the array of expression ranges:
				exprRanges.add(null);
			}

			// Go to the array element’s type spec:
			typeDesc = arrayTypeDesc.elementTypeDesc;
		}
	}

	/** Algorithm AR-4: Reconcile the definition and initialization ranges: The intention
	is to combine the declared ranges with the initialization expression ranges, and
	disallow inconsistencies. The result is an array of ranges for the array’s initial value.
	The array’s defined ranges are not affected.
	Ref: section "Array Value Assignment" of the Analyzer Design Doc.
	*/
	public Long[] reconcileArrayDefAndExprRanges(Long[] declRanges, Long[] initRanges) throws Exception {
		// Create a zero length, expandable array, array of ranges:
		ArrayList<Long> ranges = new ArrayList<Long>();

		int dim = 0;
		for (Long declRange : declRanges) { // each element (dimension) of the array of declared ranges
			dim++;

			if (declRange == null) { // the declared range is null
				ranges[subOne(dim)] = null; // mark the range null in the array of ranges.
			}

			// Compare it to the corresponding element of the array of expression ranges:
			Long initRange = null;
			if (initRanges.size() >= dim) initRange = initRanges[subOne(dim)];
			if (initRange != null) { // there is an initialization expression range at this dimension
				if (declRange.equals(initRange)) { // they are equal (they may both be null)
					ranges.add(declRange); // append the value to the array of ranges
				}
				else { // (not equal):
					if (declRange == null) { // the declared range is null (the other is not null)
						ranges.add(initRange); // append the non-null value to the array of ranges
					}
					else { // different non-null values:
						// Only allowed if init range value is 1; in that case the other value is used.
						// Assume that each element of the expression applies to the entire
						// declared range.
						if (initRange.longValue() != 1) throw new Exception("Different declared range and initialization range");
						// Append the declared range to the array of ranges; do not
						// advance the initialization expr array pos on the next iter:
						ranges.add(declRange);
					}
				}
			}
			else { // (there is not an expression range for the def range)
				if (declRange == null) { // the def range is null too
					throw new Exception("Ambiguous initial array range");
				}
				// Append the def range to the array of ranges:
				ranges.add(declRang);
			}
		}

		return ranges;
	}

	/** Algorithm AR-5: AR-5: Update the dimensions of the initial value, and populate
	as needed: Once the actual intended ranges of the array initialization have been determined,
	walk through each branch of the array literal, and populate it as needed.

	Given an array literal, and its intended dimensions Range[], call populate:
		populate(arrayLiteral, Range[], 1).
	*/
	public void populate(ParserRuleContext possibleArrayLiteral, Long[] range, int depth) throws Exception {

		long noOfElts; // number of elements (if any) in the outermost level of possibleArrayLiteral

		// Identify the array literal (if the argument is one, or reduces to one - or not):
		Array_literalContext arrayLiteral;
		if (possibleArrayLiteral instanceof ExprContext) {
			arrayLiteral = getArrayLiteral(possibleArrayLiteral); // may be null

		} else if (possibleArrayLiteral instanceof Aarray_literalContext) {
			arrayLiteral = (Array_literalContext)possibleArrayLiteral;

		} else {
			throw new Exception("Expected an array literal or an expression");
		}

		if (arrayLiteral == null) { // not an array literal: do we convert it to one?
			if (depth < range.size) {
				// Convert possibleArrayLiteral into a single element array literal.
				// Do this by annotating possibleArrayLiteral to represent the re-written literal:
				List<ExprContext> exprs = new LinkedList<ExprContext>();
				exprs.add(possibleArrayLiteral);
				Annotation annot = new ArrayLiteralAnnotation(/* an expr: */ possibleArrayLiteral, exprs);
				this.state.setArrayLiteralAnnot(possibleArrayLiteral, annot);

			} else { // at the leaf level - do nothing; note that the range[left] = 1
			}
			noOfElts = 1;

		} else { // an array literal: recurse through each element:
			List<ExprContext> exprs = arrayLiteral.expr_seq();
			noOfElts = exprs.size(); // number of elements in the array literal
			for (ExprContext expr : exprs) { // each element (if any) of arrayLiteral,
				populate(expr, range, depth+1);  // recursive
			}

			// Create and attach annotation to node, and every node up through possibleArrayLiteral:
			ParseTree node = arrayLiteral;
			for (;;) {
				Annotation annot = new ArrayLiteralAnnotation(node, exprs); // reuse expr for each annot
				this.state.setArrayLiteralAnnot(node, annot);
				if (node == possibleArrayLiteral) break;
				node = node.getParent();
			}
		}

		// Populate implicit elements, if any:
		long noOfEltsNeeded = range[subOne(depth)];
		if (noOfElts != noOfEltsNeeded) { // there are missing (implicit) elements
			assertThat(noOfElts == 1, "Expected one element");
			assertThat(possibleArrayLiteral != null, "possibleArrayLiteral is null");
			// Populate by copying:
			// Expand arrayLiteral to Range[depth], and copy arrayLiteral[1] into
			// each position of the expanded array literal:
			ArrayLiteralAnnotation annot = this.state.getArrayLiteralAnnot(possibleArrayLiteral);
			ExprContext firstElement = annot.exprs.get(subOne(1)); // may be null
			for (int i = 1; i <= noOfEltsNeeded-1; i++) {
				annot.exprs.add(firstElement);
			}

			// Note: Since we reuse the expr list for any "upper" annotations, up through the owning
			// expr node, if any, we don't need to modify those annotations - their expr
			// field automatically reflects the changes made above.
		}
	}
}
