package cliffberg.flow.analyzer;

import static cliffberg.flow.analyzer.FlowDataType.*;
import static cliffberg.flow.analyzer.FlowNonDataType.*;
import cliffberg.flow.parser.FlowParser.Value_type_specContext;
import cliffberg.symboltable.*;

import org.antlr.v4.runtime.tree.ParseTree;
import org.antlr.v4.runtime.tree.TerminalNode;

import java.util.List;
import java.util.LinkedList;
import java.util.Set;
import java.util.TreeSet;

/**
 Two type descriptors are equal if and only if,
	They both fully and identically describe the type of an object of that type.
	For those that define their own name scope:
		Their name scope fields must reference the same NameScope object.
 */

public abstract class FlowTypeDescriptor
	implements TypeDescriptor<FlowValueType>, Cloneable {
}

abstract class NonDataTypeDescriptor extends FlowTypeDescriptor {

	public boolean canBeAssignedTo(TypeDescriptor<? extends FlowValueType> otherType) {
		return false;
	}

	public NonDataTypeDescriptor duplicate() {
		try {
			return (NonDataTypeDescriptor)(this.clone());
		} catch (CloneNotSupportedException ex) {
			throw new RuntimeException(ex);
		}
	}
}

class ConduitTypeDescriptor extends NonDataTypeDescriptor {

	public FlowNonDataType getType() { return FlowConduit; }

	public boolean isA(TypeDescriptor<? extends FlowValueType> t) {
		return this.getType() == t.getType();
	}
}

/**
 For conduits that have a static type: conduits that are returned from a method have static type.
 */
class StaticConduitTypeDescriptor
	extends ConduitTypeDescriptor {

	Set<ClassOrContractTypeDescriptor> sourceTypes = null; // inbound
	Set<ClassOrContractTypeDescriptor> destTypes = null;  // outbound

	StaticConduitTypeDescriptor(
		Set<ClassOrContractTypeDescriptor> sourceTypes,
		Set<ClassOrContractTypeDescriptor> destTypes) {

		this.sourceTypes = sourceTypes;
		this.destTypes = destTypes;
	}

	/** From interface method description:
	 Return true if (and only if) something described by this type descriptor
	 can be treated as something that is described by the specified type descriptor.
	 */
	public boolean isA(TypeDescriptor<? extends FlowValueType> t) {
		/* this isA <specified> => true iff:
				this.<sourceTypes> includes all of <specified>.<sourceTypes>
				and
				this.<destTypes> includes all of <specified>.<destTypes>.
			*/
		if (! (t instanceof StaticConduitTypeDescriptor)) return false;

		StaticConduitTypeDescriptor tt = (StaticConduitTypeDescriptor)t;

		sources: for (ClassOrContractTypeDescriptor member : this.sourceTypes) {
			for (ClassOrContractTypeDescriptor mst : tt.sourceTypes) {
				if (mst.isA(member)) continue sources;  // found match
			}
			return false;
		}

		dests: for (ClassOrContractTypeDescriptor member : this.destTypes) {
			for (ClassOrContractTypeDescriptor mdt : tt.destTypes) {
				if (mdt.isA(member)) continue dests;  // found match
			}
			return false;
		}

		return true;  // found a match for each source and dest type
	}

	public Object clone() throws CloneNotSupportedException {
		StaticConduitTypeDescriptor copy = (StaticConduitTypeDescriptor)(super.clone());
		if (sourceTypes != null) {
			copy.sourceTypes = new TreeSet<ClassOrContractTypeDescriptor>();
			copy.sourceTypes.addAll(sourceTypes);
		}
		if (destTypes != null) {
			copy.destTypes = new TreeSet<ClassOrContractTypeDescriptor>();
			copy.destTypes.addAll(destTypes);
		}
		return copy;
	}

	public boolean equals(Object o) {
		if (! (o instanceof StaticConduitTypeDescriptor)) return false;
		StaticConduitTypeDescriptor td = (StaticConduitTypeDescriptor)o;
		if (((sourceTypes == null) || (td.sourceTypes == null)) && (sourceTypes != td.sourceTypes)) return false;
		if (((destTypes == null) || (td.destTypes == null)) && (destTypes != td.destTypes)) return false;
		if (! sourceTypes.equals(td.sourceTypes)) return false;
		if (! destTypes.equals(td.destTypes)) return false;
		return super.equals(o);
	}
}

class ClassOrContractTypeDescriptor extends NonDataTypeDescriptor {

	NameScope<FlowValueType, ParseTree, TerminalNode> nameScope;

	ClassOrContractTypeDescriptor(NameScope<FlowValueType, ParseTree, TerminalNode> nameScope) {
		this.nameScope = nameScope;
	}

	public FlowValueType getType() { return FlowClassOrContract; }

	public boolean isA(TypeDescriptor<? extends FlowValueType> t) {
		/* Return true iff this class or contract is identical to the specified one.
			*/

		if (! (t instanceof ClassOrContractTypeDescriptor)) return false;

		return this == t;
	}

	public Object clone() throws CloneNotSupportedException {
		return super.clone();
	}

	public boolean equals(Object o) {
		if (! (o instanceof ClassOrContractTypeDescriptor)) return false;
		ClassOrContractTypeDescriptor td = (ClassOrContractTypeDescriptor)o;
		if (! (nameScope == td.nameScope)) return false;
		return super.equals(o);
	}
}

/**
 For classes that inherit multiple other classes or implement multiple contracts.
 */
class CompositeClassOrContractTypeDescriptor extends ClassOrContractTypeDescriptor {

	Set<ClassOrContractTypeDescriptor> composedOfTypes;

	CompositeClassOrContractTypeDescriptor(NameScope<FlowValueType, ParseTree, TerminalNode> nameScope,
		Set<ClassOrContractTypeDescriptor> composedOfTypes) {
		super(nameScope);
		this.composedOfTypes = composedOfTypes;
	}
	public Object clone() throws CloneNotSupportedException {
		CompositeClassOrContractTypeDescriptor td = (CompositeClassOrContractTypeDescriptor)(super.clone());
		td.composedOfTypes = new TreeSet<ClassOrContractTypeDescriptor>();
		td.composedOfTypes.addAll(composedOfTypes);
		return td;
	}
	public boolean equals(Object o) {
		if (! (o instanceof CompositeClassOrContractTypeDescriptor)) return false;
		CompositeClassOrContractTypeDescriptor td = (CompositeClassOrContractTypeDescriptor)o;
		if (! (composedOfTypes.equals(td.composedOfTypes))) return false;
		return super.equals(o);
	}
}

class ObjectTypeDescriptor extends NonDataTypeDescriptor {

	ClassOrContractTypeDescriptor implementedClassType;

	ObjectTypeDescriptor(ClassOrContractTypeDescriptor implementedClassType) {
		this.implementedClassType = implementedClassType;
	}

	public Object clone() throws CloneNotSupportedException {
		ObjectTypeDescriptor copy = (ObjectTypeDescriptor)(super.clone());
		copy.implementedClassType = (ClassOrContractTypeDescriptor)(this.implementedClassType.clone());
		return copy;
	}

	public FlowValueType getType() { return FlowObject; }

	/**
	 Return true if this object implements the specified class or contract.
	 */
	public boolean isA(TypeDescriptor<? extends FlowValueType> td) {
		return this.implementedClassType.isA(td);
	}

	public boolean equals(Object o) {
		if (! (o instanceof ObjectTypeDescriptor)) return false;
		ObjectTypeDescriptor td = (ObjectTypeDescriptor)o;
		if (! implementedClassType.equals(td.implementedClassType)) return false;
		return super.equals(o);
	}
}

/*
 Important implementation node: A type descriptor, or user of a type descriptor, should
 never modify a NameScope that is embedded in the type descriptor. This is because name
 scopes are shared across type descriptors, for efficiency.
 */

abstract class DataTypeDescriptor extends FlowTypeDescriptor {

    FlowDataType valueType;

    DataTypeDescriptor(FlowDataType t) { this.valueType = t; }

    public FlowValueType getType() { return this.valueType; }

	public abstract boolean isIntrinsic();

	public boolean equals(Object o) {
		if (! (o instanceof DataTypeDescriptor)) return false;
		DataTypeDescriptor td = (DataTypeDescriptor)o;
		if (valueType.equals(td.valueType)) return false;
		return true;
	}

	public DataTypeDescriptor duplicate() {
		try {
			return (DataTypeDescriptor)(this.clone());
		} catch (CloneNotSupportedException ex) {
			throw new RuntimeException(ex);
		}
	}

	/**
	 Return a type descriptor that describes the intersection of this type descriptor
	 and the other one. If there is no intersection, return null. If one argument is
	 null, then return the other argument. If both are null, return null.
	 If arrays, the element types must be compatible, but not necessarily the ranges, unless
	 both are fixed length arrays, in which case their sizes must be identical.
	 */
	public DataTypeDescriptor getCommonTypeDesc(DataTypeDescriptor otherTypeDesc) {

		/*
		 If either is intrinsic, both must be - simply call FlowDataType.findCommonIntrinsicType.
		 If either is enum, both must be the same type.
		 If either is a struct type, the other must be: return one or more Struct type descs
		 	that are common to both.
		 If either is an array type, both must be.
		 */

		if (this.valueType.isIntrinsic() || otherTypeDesc.valueType.isIntrinsic())
			if (this.valueType.isIntrinsic() && otherTypeDesc.valueType.isIntrinsic())
				return new IntrinsicTypeDescriptor(
					FlowDataType.findCommonIntrinsicType(this.valueType, otherTypeDesc.valueType));
			else
				return null;

		if (this.valueType == FlowEnum || otherTypeDesc.valueType == FlowEnum)
			if (this.valueType == otherTypeDesc.valueType) return otherTypeDesc;
			else return null;

		if (this.valueType == FlowStruct || otherTypeDesc.valueType == FlowStruct)
			if (this.valueType == FlowStruct && otherTypeDesc.valueType == FlowStruct) {

				/* Find the common type(s): */
				if (this.equals(otherTypeDesc)) // the types are the same,
					return this;

				// Create an empty set of typedescs:
				Set<DataTypeDescriptor> tds = new TreeSet<DataTypeDescriptor>();
				findSharedTypes(tds, this, otherTypeDesc);
				if (tds.size() == 0) // the list is empty,
					return null;

				// Construct a StructCompositeTypeDescriptor to contain the typedescs in the list:
				StructCompositeTypeDescriptor newTypeDesc = new StructCompositeTypeDescriptor(tds);
				return newTypeDesc;

			} else
				return null;

		if (this.valueType == FlowArray || otherTypeDesc.valueType == FlowArray)
			if (this.valueType == FlowArray && otherTypeDesc.valueType == FlowArray) {

				// Element types must be the same:
				ArrayTypeDescriptor thisArrayTypeDesc = (ArrayTypeDescriptor)this;
				ArrayTypeDescriptor otherArrayTypeDesc = (ArrayTypeDescriptor)otherTypeDesc;
				if (! (thisArrayTypeDesc.elementTypeDesc.equals(otherArrayTypeDesc.elementTypeDesc))) return null;

				// If either has a dynamic size, then both must have a dynamic size:
				if ((thisArrayTypeDesc.size == null) || (otherArrayTypeDesc.size == null))
					if ((thisArrayTypeDesc.size == null) && (otherArrayTypeDesc.size == null))
						return this;
					else
						return null;

				else // neither is dynamic
					if (thisArrayTypeDesc.size == otherArrayTypeDesc.size)
						return this;
					else
						return null; // both have static sizes, but the sizes are different

			} else // neither is an array
				return null;

		throw new RuntimeException("Unrecognized type descriptor, for type " + otherTypeDesc.valueType);
	}

	private void findSharedTypes(Set<DataTypeDescriptor> tds,
		DataTypeDescriptor td1, DataTypeDescriptor td2) {

		if (td1 instanceof StructCompositeTypeDescriptor) {
			// for each typedesc of this typedesc:
			for (DataTypeDescriptor td : ((StructCompositeTypeDescriptor)td1).appendedTypeDescriptors) {
				findSharedTypes(tds, td, td2);
			}

		} else { // not a composite type
			if (td1.equals(td2)) tds.add(td1);
		}
		if (td2 instanceof StructCompositeTypeDescriptor) {
			// for each typedesc of the other typedesc:
			for (DataTypeDescriptor td : ((StructCompositeTypeDescriptor)td2).appendedTypeDescriptors) {
				findSharedTypes(tds, td, td1);
			}

		} else { // not a composite type
			if (td2.equals(td1)) tds.add(td2);
		}
	}
}

class IntrinsicTypeDescriptor
	extends DataTypeDescriptor {

	IntrinsicTypeDescriptor(FlowDataType t) { super(t); }

	public Object clone() throws CloneNotSupportedException {
		return super.clone();
	}

	public boolean isIntrinsic() { return true; }

	public boolean isA(TypeDescriptor<? extends FlowValueType> td) {
		return this.equals(td);
	}

	public boolean canBeAssignedTo(TypeDescriptor<? extends FlowValueType> otherTypeDesc) {
		if (! (otherTypeDesc instanceof DataTypeDescriptor)) return false;
		DataTypeDescriptor otherValueTypeDesc = (DataTypeDescriptor)otherTypeDesc;
		if (! otherValueTypeDesc.isIntrinsic()) return false;
		return this.valueType.automaticConversionToAllowed(otherValueTypeDesc.valueType);
	}

	public boolean equals(Object o) {
		return super.equals(o);
	}
}

class IntrinsicAliasTypeDescriptor
	extends IntrinsicTypeDescriptor {

    DeclaredEntry referencedEntry;

    IntrinsicAliasTypeDescriptor(DeclaredEntry ref, FlowDataType valueType) {
    	super(valueType);
    	this.referencedEntry = ref;
    }

	public Object clone() throws CloneNotSupportedException {
		return super.clone();
	}

	public boolean isIntrinsic() { return true; }

	public boolean isA(TypeDescriptor<? extends FlowValueType> td) {
		return this.equals(td);
	}

	public boolean canBeAssignedTo(TypeDescriptor<? extends FlowValueType> otherTypeDesc) {
		// Must be the same alias type - implicit conversions are not allowed.
		return isA(otherTypeDesc);
	}

	public boolean equals(Object o) {
		if (! (o instanceof IntrinsicAliasTypeDescriptor)) return false;
		IntrinsicAliasTypeDescriptor td = (IntrinsicAliasTypeDescriptor)o;
		if (! (referencedEntry == td.referencedEntry)) return false;
		return super.equals(o);
	}
}

class EnumTypeDescriptor extends DataTypeDescriptor {

	SymbolTable<FlowValueType, ParseTree, TerminalNode> symbolTable;

	EnumTypeDescriptor(SymbolTable<FlowValueType, ParseTree, TerminalNode> symbolTable) {
		super(FlowEnum);
    	this.symbolTable = symbolTable;
    }

	protected Object clone() throws CloneNotSupportedException {
		return super.clone();
	}

	public boolean isIntrinsic() { return false; }

	public boolean isA(TypeDescriptor<? extends FlowValueType> td) {
		return this.equals(td);
	}

	public boolean canBeAssignedTo(TypeDescriptor<? extends FlowValueType> otherTypeDesc) {
		return this.equals(otherTypeDesc);
	}

	public boolean equals(Object o) {
		if (! (o instanceof EnumTypeDescriptor)) return false;
		EnumTypeDescriptor td = (EnumTypeDescriptor)o;
		if (! (symbolTable == td.symbolTable)) return false;
		return super.equals(o);
	}
}

/**
 A struct that does not extend any other struct types.
 */
class StructTypeDescriptor extends DataTypeDescriptor {

	SymbolTable<FlowValueType, ParseTree, TerminalNode> symbolTable;

	StructTypeDescriptor(SymbolTable<FlowValueType, ParseTree, TerminalNode> symbolTable) {
    	super(FlowStruct);
    	this.symbolTable = symbolTable;
    }

	StructTypeDescriptor() {
		super(FlowStruct);
		this.symbolTable = null;
	}

	public Object clone() throws CloneNotSupportedException {
		return super.clone();
	}

	public boolean isIntrinsic() { return false; }

	public boolean isA(TypeDescriptor<? extends FlowValueType> td) {
		/* Is true if,
			Type is identical, or
			this type isA for each of the types of the specified type.
		 */
		if (! (td instanceof StructTypeDescriptor)) return false;

		if (this.symbolTable == ((StructTypeDescriptor)td).symbolTable) return true;

		if (td instanceof StructExtensionTypeDescriptor) {
			StructExtensionTypeDescriptor tde = (StructExtensionTypeDescriptor)td;
			for (DataTypeDescriptor bt : tde.borrowedTypes) {
				// If this type is any of td's borrowed types, then return true.
				if (this.isA(bt)) return true;
			}
			return false;

		} else if (td instanceof StructCompositeTypeDescriptor) {
			StructCompositeTypeDescriptor tdc = (StructCompositeTypeDescriptor)td;
			for (DataTypeDescriptor t : tdc.appendedTypeDescriptors) {
				if (this.isA(t)) return true;
			}
			return false;

		}

		throw new RuntimeException("Unexpected production");
	}

	public boolean canBeAssignedTo(TypeDescriptor<? extends FlowValueType> otherTypeDesc) {
		/* True if,
			this type isA (other type) - we can assign to that if this is a that
		 */
		return this.isA(otherTypeDesc);
	}

	public boolean equals(Object o) {
		if (! (o instanceof StructTypeDescriptor)) return false;
		StructTypeDescriptor td = (StructTypeDescriptor)o;
		if (! (this.symbolTable == td.symbolTable)) return false;
		return super.equals(o);
	}
}

/**
 A struct type that extends other struct types.
 */
class StructExtensionTypeDescriptor extends StructTypeDescriptor {

	Set<DataTypeDescriptor> borrowedTypes = null;

	StructExtensionTypeDescriptor(Set<DataTypeDescriptor> borrowedTypes) {
		super();
		this.borrowedTypes = borrowedTypes;
		// Check that the borrowed types are all struct types:
		for (DataTypeDescriptor bt : borrowedTypes) {
			if (! (bt instanceof StructTypeDescriptor)) throw new RuntimeException(
				"Type " + this.symbolTable.getName() + " can only extend struct types");
		}
	}

	StructExtensionTypeDescriptor(SymbolTable<FlowValueType, ParseTree, TerminalNode> symbolTable,
			Set<DataTypeDescriptor> borrowedTypes) {
		super(symbolTable);
		this.borrowedTypes = borrowedTypes;
	}

	public Object clone() throws CloneNotSupportedException {
		StructExtensionTypeDescriptor copy = (StructExtensionTypeDescriptor)(super.clone());
		Set<DataTypeDescriptor> newbt = null;

		if (this.borrowedTypes == null) newbt = new TreeSet<DataTypeDescriptor>();
		else newbt = this.borrowedTypes;

		copy.borrowedTypes = new TreeSet<DataTypeDescriptor>(newbt);
		return copy;
	}

	public boolean isA(TypeDescriptor<? extends FlowValueType> td) {
		/* Is true iff,
			Type is identical, or one of this type's added scopes matches the specified type.
		 */
		if (super.isA(td)) return true;
		if (this.borrowedTypes != null) {
			for (DataTypeDescriptor btd : borrowedTypes) {
				if (btd.isA(td)) return true;
			}
		}
		return false;
	}

	public boolean canBeAssignedTo(TypeDescriptor<? extends FlowValueType> otherTypeDesc) {
		return this.isA(otherTypeDesc);
	}

	public boolean equals(Object o) {
		if (! (o instanceof StructExtensionTypeDescriptor)) return false;
		StructExtensionTypeDescriptor td = (StructExtensionTypeDescriptor)o;
		if (this.borrowedTypes == null) {
			if ((td.borrowedTypes != null) && (td.borrowedTypes.size() > 0)) return false;
		}
		if (td.borrowedTypes == null) {
			if ((this.borrowedTypes != null) && (this.borrowedTypes.size() > 0)) return false;
		}
		if ((this.borrowedTypes != null) && (td.borrowedTypes != null)) {
			if (this.borrowedTypes.size() != td.borrowedTypes.size()) return false;
			for (DataTypeDescriptor btd : this.borrowedTypes) {
				if (! td.borrowedTypes.contains(btd)) return false;
			}
		}
		return super.equals(o);
	}
}

/**
 A struct type that combines multiple other struct types. This is used internally to
 elaborate types that extend types that are themselves extensions of other types.
 */
class StructCompositeTypeDescriptor extends StructTypeDescriptor {

	Set<DataTypeDescriptor> appendedTypeDescriptors;

	StructCompositeTypeDescriptor(Set<DataTypeDescriptor> tds) {
		super();
		this.appendedTypeDescriptors = tds;
	}

	public Object clone() throws CloneNotSupportedException {
		StructCompositeTypeDescriptor copy = (StructCompositeTypeDescriptor)(super.clone());
		copy.appendedTypeDescriptors = new TreeSet<DataTypeDescriptor>();
		for (DataTypeDescriptor td : appendedTypeDescriptors) {
			copy.appendedTypeDescriptors.add(td.duplicate());
		}
		return copy;
	}

	public boolean isA(TypeDescriptor<? extends FlowValueType> td) {
		/* Is true iff,
			Type is identical, or
			All of the types of the specified composite type are implemented by this type.
			I.e., for each type of the specified type, this.isA(that).
		 */
		if (super.isA(td)) return true;

		if (td instanceof StructCompositeTypeDescriptor) {
			StructCompositeTypeDescriptor ctd = (StructCompositeTypeDescriptor)td;

			/* For each type t2 that is implemented by the composite type, verify that at
				least one of this type's composing types satisfies isA for the t2 composing type: */
			for (DataTypeDescriptor t2 : ctd.appendedTypeDescriptors) {
				if (super.isA(t2)) continue;
				boolean found = false;
				for (DataTypeDescriptor t : appendedTypeDescriptors) {
					if (t.isA(t2)) {
						found = true;
						break;
					}
				}
				if (! found) return false;
			}

		} else if (td instanceof StructExtensionTypeDescriptor) {
			StructExtensionTypeDescriptor setd = (StructExtensionTypeDescriptor)td;

			/* Verify that at least one of this type's composing types satisfies isA for setd:
				*/
			if (super.isA(setd)) return true;
			for (DataTypeDescriptor t : appendedTypeDescriptors) {
				if (t.isA(setd)) return true;
			}
			return false;

		} else {
			/* Other type is not composite or an extension:
				verify that each of this type's composing types is satisfied by isA
				*/
			for (DataTypeDescriptor t : appendedTypeDescriptors) {
				if (! t.isA(td)) return false;
			}
		}

		return true;
	}

	public boolean canBeAssignedTo(TypeDescriptor<? extends FlowValueType> otherTypeDesc) {
		return this.isA(otherTypeDesc);
	}

	public boolean equals(Object o) {
		if (! (o instanceof StructCompositeTypeDescriptor)) return false;
		StructCompositeTypeDescriptor other = (StructCompositeTypeDescriptor)o;
		if (other.appendedTypeDescriptors.size() != appendedTypeDescriptors.size()) return false;
		for (DataTypeDescriptor apptd : appendedTypeDescriptors) {
			if (! other.appendedTypeDescriptors.contains(apptd)) return false;
		}
		return super.equals(o);
	}
}

class ArrayTypeDescriptor extends DataTypeDescriptor {

	DataTypeDescriptor elementTypeDesc; // may be null (if the array is of size 0)

	Long size; // if null, then the array is dynamic

	ArrayTypeDescriptor(DataTypeDescriptor elementTypeDesc, Long size) {
    	super(FlowArray);
    	this.elementTypeDesc = elementTypeDesc;
    	this.size = size;
    }

	public Object clone() throws CloneNotSupportedException {
		ArrayTypeDescriptor copy = (ArrayTypeDescriptor)(super.clone());
		copy.elementTypeDesc = (DataTypeDescriptor)(this.elementTypeDesc.duplicate());
		copy.size = this.size;
		return copy;
	}

	public boolean isIntrinsic() { return false; }

	public boolean isA(TypeDescriptor<? extends FlowValueType> td) {
		/* true if,
			The elements types satisfy isA for the other type;
			size must be the same.
		 */
		if (! (td instanceof ArrayTypeDescriptor)) return false;
		ArrayTypeDescriptor other = (ArrayTypeDescriptor)td;
		if (this.size != other.size) return false;
		if (! elementTypeDesc.isA(other.elementTypeDesc)) return false;
		return true;
	}

	public boolean canBeAssignedTo(TypeDescriptor<? extends FlowValueType> otherTypeDesc) {
		/* true if,
			this is a (other type).
			this array's element type satisfies isA for the other type, AND,
				the other type is dynamic.
		 */
		if (this.isA(otherTypeDesc)) return true;
		if (! (otherTypeDesc instanceof ArrayTypeDescriptor)) return false;
		ArrayTypeDescriptor other = (ArrayTypeDescriptor)otherTypeDesc;
		if (this.elementTypeDesc.isA(other.elementTypeDesc) && (other.size == null)) return true;
		return false;
	}

	public boolean equals(Object o) {
		if (! (o instanceof ArrayTypeDescriptor)) return false;
		ArrayTypeDescriptor td = (ArrayTypeDescriptor)o;
		if (! (elementTypeDesc.equals(td.elementTypeDesc))) return false;
		if (! size.equals(td.size)) return false;
		return super.equals(o);
	}
}
