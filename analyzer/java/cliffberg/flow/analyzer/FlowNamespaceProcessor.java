package cliffberg.flow.analyzer;

import cliffberg.flow.parser.FlowParser;
import cliffberg.flow.parser.FlowLexer;
import cliffberg.symboltable.Analyzer;
import cliffberg.symboltable.NameScope;
import cliffberg.symboltable.NamespaceProcessorBase;

import java.io.Reader;

import org.antlr.v4.runtime.tree.ParseTree;
import org.antlr.v4.runtime.tree.ParseTreeWalker;
import org.antlr.v4.runtime.UnbufferedCharStream;
import org.antlr.v4.runtime.UnbufferedTokenStream;
import org.antlr.v4.runtime.TokenStream;
import org.antlr.v4.runtime.CommonToken;
import org.antlr.v4.runtime.CommonTokenFactory;
import org.antlr.v4.runtime.tree.ParseTreeListener;

public class FlowNamespaceProcessor extends NamespaceProcessorBase {

	public FlowNamespaceProcessor(Analyzer analyzer) {
		super(analyzer);
	}

	public NameScope processNamespace(Reader reader, boolean parseOnly) throws Exception {
		
		// Parse the input and generate an AST.
		UnbufferedCharStream charStream = new UnbufferedCharStream(reader, 100);
		FlowLexer lexer = new FlowLexer(charStream);
		lexer.setTokenFactory(new CommonTokenFactory(true));
		TokenStream tokens = new UnbufferedTokenStream<CommonToken>(lexer);
		FlowParser parser = new FlowParser(tokens);
		ParseTree tree = parser.namespace();
		int numErrors = parser.getNumberOfSyntaxErrors();
		System.err.println(numErrors + " syntax errors");
		if (numErrors > 0) throw new Exception(numErrors + " syntax errors");
		getAnalyzer().getState().getASTs().add(tree);

		if (! parseOnly) {
			// Analyze the AST.
			ParseTreeWalker walker = new ParseTreeWalker();
			walker.walk((ParseTreeListener)(getAnalyzer()), tree);
	
			return getAnalyzer().getNamespaceNamescope();
		} else 
			return null;
	}
}
