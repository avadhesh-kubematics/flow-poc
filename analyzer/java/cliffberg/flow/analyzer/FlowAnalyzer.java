package cliffberg.flow.analyzer;

import static cliffberg.flow.analyzer.FlowDataType.*;
import static cliffberg.flow.analyzer.ParseTreeUtilities.*;

import cliffberg.flow.parser.FlowBaseListener;
import static cliffberg.flow.parser.FlowParser.*;

import cliffberg.symboltable.Annotation;
import cliffberg.symboltable.ExprAnnotation;
import cliffberg.symboltable.SymbolTable;
import cliffberg.symboltable.SymbolEntry;
import cliffberg.symboltable.ImportHandler;
import cliffberg.symboltable.Analyzer;
import cliffberg.symboltable.NameResolution;
import cliffberg.symboltable.NameScopeEntry;
import cliffberg.symboltable.NameScope;
import cliffberg.symboltable.IdentHandler;
import cliffberg.symboltable.DeclaredEntry;
import cliffberg.symboltable.SymbolEntryPresent;
import cliffberg.symboltable.SymbolWrapper;
import cliffberg.symboltable.VisibilityChecker;
import cliffberg.symboltable.PublicVisibilityChecker;
import cliffberg.symboltable.UnknownTypeException;

import static cliffberg.utilities.AssertionUtilities.*;

import org.antlr.v4.runtime.Token;
import org.antlr.v4.runtime.ParserRuleContext;
import org.antlr.v4.runtime.tree.TerminalNode;
import org.antlr.v4.runtime.tree.ErrorNode;
import org.antlr.v4.runtime.tree.ParseTree;

import java.util.List;
import java.util.LinkedList;
import java.util.Stack;
import java.util.Set;
import java.util.TreeSet;
import java.util.Map;
import java.util.Queue;

/**
 Ref Analyzer Design,
 https://drive.google.com/open?id=1Lh4bCMkbofyIIs2KhGDXp8yHaVo3RsnXB6Ek2yKL0lM
 *
 This class defines parameterized types so that it is not too dependent on Antler.
 Otherwise, there is no reason not to use the types defined by Antlr.
 */
public class FlowAnalyzer<Type, Node extends ParseTree, Start extends Node, AOidRef extends Node,
	TId extends Node>  	// TId must be an Antlr TerminalNode.
		extends FlowBaseListener
		implements Analyzer {

	public FlowAnalyzer(FlowCompilerState state, FlowImportHandler importHandler, int maxErrors) {

		this.state = state;
		this.maxNoOfErrors = maxErrors;
		this.importHandler = importHandler;
		this.nameResolver = new NameResolution<Type, Node, Start, AOidRef, TId>(
			state, symbolWrapper) {
			public ImportHandler getImportHandler() {
				return FlowAnalyzer.this.getImportHandler();
			}

			public NameScopeEntry getEnclosingScopeEntry() {
				return FlowAnalyzer.this.getEnclosingScopeEntry();
			}
		};

		this.visibilityChecker = new PublicVisibilityChecker(NamespaceContext.class);
		this.vTableFactory = new VTableFactory<Type, Node, TId>();

		if (state.getGlobalScope() == null) {
			state.setGlobalScope(nameResolver.pushNameScope(
				new NameScope<Type, Node, TId>("Global", null, null, symbolWrapper)));
		}

		this.parseTreeUtils = new ParseTreeUtilities<Node, TId>();
	}


	private FlowCompilerState state;
	private NameResolution<Type, Node, Start, AOidRef, TId> nameResolver;
	private NameScope<Type, Node, TId> namespaceNamescope;
	private FlowImportHandler importHandler;
	private String namespaceName;
	private NamespaceContext namespaceNode;
	private VisibilityChecker visibilityChecker;
	private ParseTreeUtilities<Node, TId> parseTreeUtils;
	private VTableFactory vTableFactory;



	/*
	 Types and lists for managing additional passes; instead of a full pass,
	 each list is traversed:
	 */

	//private List<FunctionCallRef> funcCallRefs = new LinkedList<FunctionCallRef>();
	private List<MethodCallRef> methodCallRefs = new LinkedList<MethodCallRef>();
	private List<QualNameRef> qualNameRefs = new LinkedList<QualNameRef>();
	private List<ValueDefRef> valueDefRefs = new LinkedList<ValueDefRef>();
	private List<ValueTypeSpecRef> valueTypeSpecRefs = new LinkedList<ValueTypeSpecRef>();
	private List<ValueTypeSpecScopeRef> valueTypeSpecScopes = new LinkedList<ValueTypeSpecScopeRef>();
	private List<ValueContext> values = new LinkedList<ValueContext>();
	private List<ExprContext> exprs = new LinkedList<ExprContext>();
	private List<ArrayEltTypeSpecRef> arrayDeclRefs = new LinkedList<ArrayEltTypeSpecRef>();
	private Map<Value_type_specContext, ExprContext> arrayValueTypeSpecInitExprs = new HashMap<Value_type_specContext, ExprContext>();
	private Map<Value_type_specContext, DeclaredEntry<Type, Node, TId>> implicitlyResolvesTypeSpecs = new
		HashMap<value_type_specContext, DeclaredEntry<Type, Node, TId>>();
	private Queue<Array_literalContext> arrayLiteralStack = new LinkedList<Array_literalContext>();
	private int arrayLiteralNoOfDims = 0;


	private class ScopedRef {
		NameScope<Type, Node, TId> scopeOfUse;
		ScopedRef(NameScope<Type, Node, TId> scopeOfUse) {
			this.scopeOfUse = scopeOfUse;
		}
	}

	/*
	private class FunctionCallRef extends ScopedRef {
		ExprContext expr;  // the target of the call
		Function_callContext functionCall;
		TId functionName;
		FunctionCallRef(ExprContext expr, Function_callContext functionCall, TId functionName,
			NameScope<Type, Node, TId> scopeOfUse) {
			super(scopeOfUse);
			this.expr = expr;
			this.functionCall = functionCall;
			this.functionName = functionName;
		}
	}
	*/

	private class MethodCallRef extends ScopedRef {

		Method_call_stmtContext methodCallStmt;
		Method_callContext methodCall;
		Arg_sectionContext argSection;
		Value_defContext assignValueDef;  // may be null
		Qual_nameContext targetQualName;  // target of the call - may be null
		TId id;  // method name
		Supports_claimContext supportsClaim; // may be null; qualName and supportsClaim may not both be present

		MethodCallRef(Method_call_stmtContext methodCallStmt, Method_callContext methodCall,
			Value_defContext assignValueDef, TId id, Arg_sectionContext argSection,
			Supports_claimContext supportsClaim, NameScope<Type, Node, TId> scopeOfUse) {
			super(scopeOfUse);
			this.methodCallStmt = methodCallStmt;
			this.methodCall = methodCall;
			this.assignValueDef = assignValueDef;
			this.targetQualName = null;
			this.id = id;
			this.argSection = argSection;
			this.supportsClaim = supportsClaim;
		}

		MethodCallRef(Method_call_stmtContext methodCallStmt, Method_callContext methodCall,
			Value_defContext assignValueDef, Qual_nameContext targetQualName, TId id,
			Arg_sectionContext argSection,
			NameScope<Type, Node, TId> scopeOfUse) {
			super(scopeOfUse);
			this.methodCallStmt = methodCallStmt;
			this.methodCall = methodCall;
			this.assignValueDef = assignValueDef;
			this.targetQualName = targetQualName;
			this.id = id;
			this.argSection = argSection;
			this.supportsClaim = null;
		}
	}

	/**
	 Encapsulate a qual_name and the name scope in which it occurs.
	 */
	private class QualNameRef extends ScopedRef {
		Qual_nameContext qualName;
		QualNameRef(Qual_nameContext qualName, NameScope<Type, Node, TId> scopeOfUse) {
			super(scopeOfUse);
			this.qualName = qualName;
		}
	}

	/**
	 Encapsulate a value_def and the name scope in which it occurs.
	 */
	private class ValueDefRef extends ScopedRef {
		Value_defContext valueDef;
		ValueDefRef(Value_defContext valueDef, NameScope<Type, Node, TId> scopeOfUse) {
			super(scopeOfUse);
			this.valueDef = valueDef;
		}
	}

	private class ValueTypeSpecRef extends ScopedRef {
		DeclaredEntry<Type, Node, TId> entry;
		Value_type_specContext valueTypeSpec;
		ValueTypeSpecRef(NameScope<Type, Node, TId> scopeOfUse, DeclaredEntry<Type, Node, TId> entry,
			Value_type_specContext valueTypeSpec) {
			super(scopeOfUse);
			this.entry = entry;
			this.valueTypeSpec = valueTypeSpec;
		}
	}

	private class ValueTypeSpecScopeRef extends ScopedRef {
		Qual_nameContext qualName;
		NameScope<Type, Node, TId> structScope;
		Struct_type_spec_scopeContext valueTypeSpecScope;
		ValueTypeSpecScopeRef(Struct_type_spec_scopeContext valueTypeSpecScope,
			Qual_nameContext qualName, NameScope<Type, Node, TId> structScope, NameScope<Type, Node, TId> scopeOfUse) {
			super(scopeOfUse);
			this.qualName = qualName;
			this.structScope = structScope;
			this.valueTypeSpecScope = valueTypeSpecScope;
		}
	}

	private class ArrayEltTypeSpecRef {
		Array_elt_type_specContext arrayEltTypeSpec;
		List<Array_def_specContext> arrayDefSpecs;
		Value_type_specContext valueTypeSpec;
		ArrayEltTypeSpecRef(Array_elt_type_specContext arrayEltTypeSpec, List<Array_def_specContext> arrayDefSpecs) {
			this.arrayEltTypeSpec = arrayEltTypeSpec;
			this.arrayDefSpecs = arrayDefSpecs;
			this.valueTypeSpec = (Value_type_specContext)(arrayEltTypeSpec.getParent());
		}
	}

	private void addOutermostArrayLiteral(Array_literalContext arrayLiteral, int dims) {
		this.outermostArrayLiteralRefs.add(new OutermostArrayLiteralRef(arrayLiteral, dims));
	}

	public List<OutermostArrayLiteralRef> getOutermostArrayLiteralRefs() {
		return this.outermostArrayLiteralRefs;
	}

	private List<OutermostArrayLiteralRef> outermostArrayLiteralRefs = new LinkedList<OutermostArrayLiteralRef>();

	class OutermostArrayLiteralRef implements Annotation {
		Array_literalContext arrayLiteral;
		int noOfDimensions;
		OutermostArrayLiteralRef(Array_literalContext arrayLiteral, int noOfDimensions) {
			this.arrayLiteral = arrayLiteral;
			this.noOfDimensions = noOfDimensions;
		}
	}







	/*
	 Convenience methods:
	 */

	private FlowImportHandler getImportHandler() { return importHandler; }

	private NameResolution<Type, Node, Start, AOidRef, TId> getNameResolver() { return nameResolver; }

	private NameScope<Type, Node, TId> getCurrentNameScope() { return nameResolver.getCurrentNameScope(); }

	private SymbolWrapper<Node> getSymbolWrapper() { return this.state.getSymbolWrapper(); }

	private FlowSymbolWrapper getFlowSymbolWrapper() { return (FlowSymbolWrapper)(nameResolver.getSymbolWrapper()); }

	private String getText(ParseTree node) { return getFlowSymbolWrapper().getText(node); }

	private int getLine(ParseTree node) { return getFlowSymbolWrapper().getLine(node); }

	private int getPos(ParseTree node) { return getFlowSymbolWrapper().getPos(node); }

	private ExprAnnotation<Type, Node> getVal(Node node) {
		return nameResolver.getVal(node);
	}

	private void addLinePosExc(ParseTree node, String msg) {
		(new Exception(msg)).printStackTrace(System.err);  // debug
		this.state.addLinePosExc(parseTreeUtils.asNode(node), msg);
	}

	private void addLinePosExc(ParseTree node, Exception ex) {
		ex.printStackTrace(System.err);  // debug
		this.state.addLinePosExc(parseTreeUtils.asNode(node), ex);
	}

	private void addLinePosExc(ParseTree node, String msg, Exception ex) {
		ex.printStackTrace(System.err);  // debug
		this.state.addLinePosExc(parseTreeUtils.asNode(node), msg, ex);
	}



	/*
	 Methods defined in Analyzer interface, which must be implemented:
	 */

	@Override public FlowCompilerState getState() {
		return this.state;
	}

	@Override public NameScopeEntry<Type, Node, TId> getEnclosingScopeEntry() {
		return this.namespaceNamescope.getSelfEntry();
	}

	@Override public NameScope<Type, Node, TId> getNamespaceNamescope() {
		return this.namespaceNamescope;
	}



	/*
	 Depth-first parse tree traversal methods defined in FlowListener:
	 */


	@Override public void enterNamespace(NamespaceContext ctx) {
		System.err.println("FlowAnalyer.enterNamespace:");

		// Obtain the namespace name.
		// Ref LRM, sections "Namespaces" and "Internal Namespaces".

		Namespace_declContext namespaceDecl = ctx.namespace_decl();
		List<TId> idents = parseTreeUtils.asTIds(namespaceDecl.Tident());

		List<Slash_nameContext> slashNames = namespaceDecl.slash_name();
		List<TId> slashNameIdents = new LinkedList<TId>();
		for (Slash_nameContext slashName : slashNames) {
			slashNameIdents.add(parseTreeUtils.asTId(slashName.Tident()));
		}

		String name = nameResolver.createNameFromPath(idents);
		if (slashNameIdents.size() > 0) {
			String slashName = nameResolver.createNameFromPath(slashNameIdents);
			name = name + "/" + slashName;
		}

		this.namespaceName = name;
		this.namespaceNode = ctx;
		System.err.println("Processing namespace " + name);
	}

	@Override public void enterNamespace_scope(Namespace_scopeContext ctx) {

		assertThat(this.namespaceName != null, "this.namespaceName is null");  // debug
		assertThat(this.namespaceNode != null, "this.namespaceNode is null");  // debug

		NameScope<Type, Node, TId> globalScope = getCurrentNameScope();
		assertThat(globalScope != null, "globalScope is null");  // debug
		assertThat(globalScope == state.getGlobalScope(), "globalScope is not correct");  // debug

		// Create a new name scope and push it into the scope stack.
		this.namespaceNamescope = nameResolver.createNameScope(
			this.namespaceName, parseTreeUtils.asNode(this.namespaceNode));

		cliffberg.symboltable.SymbolTable namespaceSymbolTable = this.namespaceNamescope.getSymbolTable();  // debug
		assertThat(namespaceSymbolTable.getName() != null,  // debug
			"namespace symbol table name is null");  // debug

		// Locate the first TId of the name scope name.
		Namespace_declContext namespaceDecl = this.namespaceNode.namespace_decl();
		List<TId> idents = parseTreeUtils.asTIds(namespaceDecl.Tident());

		// Create an entry for the namespace in the global symbol table.
		NameScopeEntry nameScopeEntry =
			new NameScopeEntry(this.namespaceNamescope, this.namespaceName, globalScope);
		try {
			nameResolver.addSymbolEntry(nameScopeEntry, idents.get(0),
				this.namespaceName, globalScope, false);
		} catch (SymbolEntryPresent ex) {
			throw new RuntimeException(ex);
		}
		this.namespaceNamescope.setSelfEntry(nameScopeEntry);
	}

	@Override public void exitNamespace_scope(Namespace_scopeContext ctx) {

		TypeSpecAnalyzer<Type, Node, Start, AOidRef, TId> typeSpecAnalyzer =
			new TypeSpecAnalyzer<Type, Node, Start, AOidRef, TId>(this.nameResolver,
				this.state, this.symbolWrapper);


		/* 2. Name scope fixes: Revisit Nodes that have special name resolution context rules,\
			to complete their contexts. This must be done before we begin name resolution.

			Structs:
			Within a struct definition, one can reference inherited fields by their
			simple names. Thus, we must append the symbol table of each inherited
			struct type to the struct's symbol table. */

		for (ValueTypeSpecScopeRef vtsScope : valueTypeSpecScopes) {
			SymbolTable<Node, TId> structTable = vtsScope.structScope.getSymbolTable();

			// Resolve the referenced struct type:
			List<TerminalNode> path = vtsScope.qualName.Tident();
			SymbolEntry entry = nameResolver.resolveSymbol(parseTreeUtils.asTIds(path), vtsScope.scopeOfUse,
				this.visibilityChecker);
			assertIsA(entry, NameScopeEntry.class);

			// Append the struct type symbol table to the struct instance symbol table:
			NameScopeEntry nameScopeEntry = (NameScopeEntry)entry;
			NameScope<Type, Node, TId> structTypeScope = nameScopeEntry.getOwnedScope();
			SymbolTable<Node, TId> structTypeTable = structTypeScope.getSymbolTable();
			System.err.println("In namespace " + namespaceName +
				", in scope " + vtsScope.scopeOfUse.getName() +
				", at line " + nameResolver.getSymbolWrapper().getLine(
					vtsScope.structScope.getNodeThatDefinesScope()) +
				", appending table " + structTypeTable.getName() +
				" to table " + structTable.getName());  // debug
			structTable.appendTable(structTypeTable);
		}

		/* 3. Assign types to all expressions, and evaluate those that are static:
			Attempt to evaluate statically determinable values and expressions, and
			assign explicit types to them. */

		StaticEvaluator<Type, Node, Start, AOidRef, TId> staticEvaluator =
			new StaticEvaluator<Type, Node, Start, TId>(this.state, this.typeSpecAnalyzer, this.nameResolver,
				this.visibilityChecker, this.vTableFactory);

		for (ValueContext value : this.values) {
			staticEvaluator.evaluate(value);
		}

		for (ExprContext expr : this.exprs) {
			staticEvaluator.evaluate(expr);
		}

		/* 4. Treat some value_defs as declarations:
			Iterate through list of possible declarations, and create a declaration
			for each that could syntactically serve as a declaration, but for which there
			is no superceding declaration. (see enterValue_def). If an initialization expression
			is needed to determine the type, but the expression was not evaluatable given the declarations
			available to it, it is an error. */

		for (ValueDefRef valueDefRef : valueDefRefs) {

			// Try to resolve the name.
			System.err.println("At line " + valueDefRef.valueDef.value_decl().Tident().getSymbol().getLine() + // debug
				", trying to resolve " + valueDefRef.valueDef.value_decl().Tident().getText()); // debug
			String name = getValueDefName(valueDefRef.valueDef);
			SymbolEntry entry = nameResolver.identifySymbol(name, valueDefRef.scopeOfUse,
				this.visibilityChecker);

			if (entry == null) {  /* could not resolve it using prior (explicit) value definitions (those
					with the keyword "value" or a value_type_spec */
				System.err.println("info: pass 1 did not define " + name); 			// debug

				// Analyze the value_def to determine if it is actually a value definition:
				entry = resolveImplicitly(valueDefRef.valueDef);
				if (entry == null) {
					addLinePosExc(valueDefRef.valueDef, "Unable to resolve " + name);
					continue;
				}

				Value_type_specContext valueTypeSpec = valueDefRef.valueDef.value_decl().value_type_spec();
				if (valueTypeSpec == null) {
					throwLinePosExc(valueDefRef.valueDef, "No value_type_spec found");
				}

				implicitlyResolvesTypeSpecs.put(valueTypeSpec, entry);
			}
		}


		/* 5. Analyze all value_type_specs and determine their types, applying explicit
		 static initilization expr types where needed to determine the symbol's type. */

		for (ValueTypeSpecRef valueTypeSpecRef : valueTypeSpecRefs) {

			/* DeclaredEntry field might be null if the declaration is implicit; if so, we need to
				identify the entry. */
			if (valueTypeSpecRef.entry == null) {
				valueTypeSpecRef.entry = implicitlyResolvesTypeSpecs.get(valueTypeSpecRef.valueTypeSpec);
				assertThat(valueTypeSpecRef.entry != null, "Implicitly resolves entry not set");
			}

			try {
				typeSpecAnalyzer.analyzeTypeSpec(valueTypeSpecRef.entry, valueTypeSpecRef.valueTypeSpec);
			} catch (LinePosException ex) {
				throw ex; // if a LinePosExc is being thrown instead of logged, it means an internal error
			} catch (Exception ex2) {
				addLinePosExc(valueTypeSpecRef.valueTypeSpec, ex2.getMessage());
			}
		}


		/* 6. Analyze array literals:
		 Algorithm AR-1.1: (Ref. Analyzer Design, "Type Analysis of Array Literals".)
		 Analyze all outermost array literals - those that are not themselves elements of
		 an array literal. */

		FlowArrays<Type, Node, Start, TId> flowArrays = new FlowArrays<Type, Node, Start, TId>(this.state);
		for (OutermostArrayLiteralRef arrayLiteralRef : outermostArrayLiteralRefs) {
			int noOfDims = arrayLiteralRef.noOfDimensions;
			Array_literalContext arrayLiteral = arrayLiteralRef.arrayLiteral;
			Integer[] range = new Integer[noOfDims];
			Arrays.fill(range, null);
			flowArrays.analyzeArrayLiteral(arrayLiteral, range, 1);
			flowArrays.updateArrayLiteralSizes(arrayLiteral, range, 1);
		}

		/* 7. Analyze array size initializations:
		 Reconcile array def ranges with initial value ranges. */

		for (ArrayEltTypeSpecRef arrayDeclRef : arrayDeclRefs) {

			// AR-2:
			Long[] defRanges = flowArrays.getArrayDefRanges(arrayDeclRef.arrayDefSpecs);

			// AR-3:
			ExprContext initEexpr = this.arrayValueTypeSpecInitExprs.get(arrayDeclRef.valueTypeSpec);
				// set during first pass, in exitInitialization()
			if (initExpr == null) {
				addLinePosExc(arrayDeclRef.valueTypeSpec, "An array value must be initialized");
				continue;
			}
			Long[] initRanges = flowArrays.getArrayExprRanges(initExpr);

			try {
				// AR-4:
				Long[] ranges = flowArrays.reconcileArrayDefAndExprRanges(defRanges, initRanges);

				// AR-5: Update the dimensions of the initial value, and populate as needed:
				flowArrays.populate(initExpr, ranges, 1);

			} catch (Exception ex) {
				addLinePosExc(arrayDeclRef.valueTypeSpec, ex.getMessage());
				continue;
			}
		}


		/* Resolve function references. */




		/* ....Conduit type analysis */


		/* Resolve method references. */

		next_method_call: for (MethodCallRef methodCallRef : this.methodCallRefs) {

			/*
				method_call				: meth_call_action arg_section ;
				meth_call_action		: ( qual_name Tperiod )? Tident
										| supports_claim Tperiod Tident // target must be a conduit or an object
										;
				supports_claim			: Tl_paren Tident Tsupports qual_name Tr_paren ;
				arg_section				: Tl_paren act_arg_seq? Tr_paren ;
				*/

			TId id = methodCallRef.id;
			String methodName = id.getText();

			Set<SymbolEntry<Type, Node, TId>> methodEntries = new TreeSet<SymbolEntry<Type, Node, TId>>();
			List<NameScope<Type, Node, TId>> searchScopes = new TreeSet<NameScope<Type, Node, TId>>();

			if (methodCallRef.supportsClaim != null) { // target must be a conduit or an object

				/* Method call is of the form,
					'(' Tident-1 'supports' qual_name ')' '.' Tident-2 arg_section
					*/

				/* Identify Tident-1 (the target conduit or object) within the current scope,
					and appended or any enclosing scope. */
				TId id1 = methodCallRef.supportsClaim.Tident();
				SymbolEntry<Type, Node, TId> id1Entry = getRef(id1);
				if (id1Entry == null) {
					id1Entry = this.nameResolver.identifySymbolLocally(id1);
					if (id1Entry == null) {
						addLinePosExc(id1, "Unable to identify symbol");
						continue next_method_call;
					}
				}

				/* Verify that Tident-1 refers to a conduit or object. */
				assertIsA(id1Entry, DeclaredEntry);
				Node id1DefNode = id1Entry.getDefiningNode();
				if (! (id1DefNode instanceof Conduit_specContext) ||
					(id1DefNode instanceof Object_create_specContext)) {
					addLinePosExc(id1, "Not a conduit or object");
					continue next_method_call;
				}

				/* Verify semantic rule: The target conduit or object must be local to the containing object: */
				NameScope<Type, Node, TId> enclClassDefScope = getEnclosingClassScope(id1);
				if (enclClassDefScope == null) {
					addLinePosExc(conOrObjId, "No enclosing class found");
					continue next_method_call;
				}
				if (! (isWithinScopeHierarchy(conOrObjEntry, enclClassDefScope))) {
					addLinePosExc(conOrObjId,
						"A method call may only be performed on conduits or objects within the same class");
					continue next_method_call;
				}

				/* Identify qual_name (claimed type) per rules for qualified names. */
				//	supports_claim : Tl_paren Tident Tsupports qual_name Tr_paren ;
				Qual_nameContext claimedTypeName = methodCallRef.supportsClaim.qual_name();
				SymbolEntry<Type, Node, TId> claimedTypeEntry = this.nameResolver.resolveAndBindSymbol(
					claimedTypeName.Tident(), methodCallRef.scopeOfUse, this.visibilityChecker);
				if (claimedTypeEntry == null) {
					addLinePosExc(claimedTypeName, "Not found");
					continue next_method_call;
				}

				/* Obtain the claimed type's type descriptor: */
				TypeDescriptor claimedTypeDesc = claimedTypeEntry.getTypeDesc();
				if (claimedTypeDesc == null) {
					// Attempt to resolve the type:
					try {
						claimedTypeDesc = this.typeSpecAnalyzer.analyzeEntryType(claimedTypeEntry);
					} catch (Exception ex) {
						addLinePosExc(claimedTypeName.Tident(), ex.getMessage());
						continue next_method_call;
					}
				}

				/* Verify that the claimed type name is a name scope: */
				if (! claimedTypeEntry instanceof NameScopeEntry) {
					addLinePosExc(methodCallRef.supportsClaim, "Not a name scope");
					continue next_method_call;
				}

				/* Verify that qual_name refers to a class or a contract. */
				NameScope<Type, Node, TId> typeScope = ((NameScopeEntry)claimedTypeEntry).getOwnedScope();
				Node typeScopeNode = typeScope.getNodeThatDefinesScope();
				if (! (typeScopeNode instanceof Class_defContext ||
						typeScopeNode instanceof Contract_defContext)) {
					addLinePosExc(methodCallRef.supportsClaim, "Not a class or contract");
					continue next_method_call;
				}

				/* Annotate Tident-1 with a runtime type checker, to verify that Tident
				implements the type specified by qual_name. */

				// Determine if the claim can be proven statically, or if a dynamic type checker
				// must be inserted:
				TypeDescriptor targetType = this.typeSpecAnalyzer.analyzeEntryType(conOrObjEntry);
				if (targetType == null) {
					addLinePosExc(conOrObjId, "Cannot resolve type");
					continue next_method_call;
				}
				if (targetType.canBeAssignedTo(claimedTypeDesc)) {
					typeDesc = claimedTypeDesc;

				} else { /* It might support it dynamically, so we must insert a dynamic type checker,
					and statically assume that the assertion is correct.
					*/

					// Statically assume that the target supports the claimed type.
					typeDesc = claimedTypeDesc;

					/* Tag the supports_claim with an annotation that defines the dynamic
						type checker that must be called whenever the call action is performed: */

					this.state.addDynamicTypeChecker(conOrObjId, claimedTypeDesc);
				}

				/* Collect all of the scopes that are aggregated by the qual_name type:
					set searchScopes to reference those. They must be class or contract types. */

				NameScopeUtils.assembleNameScopeFromTypes(claimedTypeDesc, searchScopes, true);

			} else { /* Method call is of the form,
						( qual_name '.' )? Tident arg_section
						*/

				if (methodCallRef.qualName != null) { // method reference has a prefix

					/* Identify the method name in the context of the qualfied name. The qualified name
						must refer to an enclosing or appended class context.
						Allow for method overloading. */

					// Get the last element of the qualified name:
					List<TId> qualNameIds = methodCallRef.targetQualName.Tident();
					TId name = qualNameIds.get(qualNameIds.size()-1);

					// Get the last element's type desc. We are expecting it to be a class, contract, or conduit type:
					TypeDescriptor targetTypeDesc = this.state.getTypeDesc(name);

					if (targetTypeDesc == null) {

						// Attempt to resolve the target:
						SymbolEntry<Type, Node, TId> targetEntry = this.nameResolver.resolveAndBindSymbol(
							qualNameIds, methodCallRef.scopeOfUse, this.visibilityChecker);

						if (targetEntry == null) {
							addLinePosExc(name, "No type descriptor found");
							continue;
						}

						/* Attempt to determine the type of the target. It should be an object
							or a conduit.
								class_def				: concurrent_spec? Tclass Tident is_seq?
									Tl_curl_br class_scope Tr_curl_br
								contract_def			: Tcontract Tident is_seq?
									Tl_curl_br contract_def_scope Tr_curl_br
								conduit_spec			: Tconduit Tident? elasticity? cleanup_handler? ;  // name can be a class or an object
							*/
						FlowTypeDescriptor targetTypeDesc = null;
						try { targetTypeDesc = typeSpecAnalyzer.analyzeType(targetEntry); }
						catch (Exception ex) {
							addLinePosExc(id, ex.getMessage());
							continue;
						}
					}

					/* Assemble the set of search scopes referenced by the target type: */

					if (targetTypeDesc instanceof ClassOrContractTypeDescriptor) {
						/* Attempt to resolve the method name in the name scope of the target's
							class(es) and/or contract(s). Need to include all derived classes, since
							method binding is virtual. */

						/* (See also the method,
								analyzeType(SymbolEntry<Node, TId>, Struct_type_specContext, Value_type_specContext)
							in TypeSpecAnalyzer.) */

						searchScopes = NameScopeUtils.assembleNameScopeFromTypes(targetTypeDesc, false);

					} else if (targetTypeDesc instanceof ConduitTypeDescriptor) {

						/* The conduit is defined to be able to connect to a classs or contract. */

						if (targetTypeDesc instanceof StaticConduitTypeDescriptor) {
							// Identify all of the name scopes owned or appended by the conduit's target types:
							List<ClassOrContractTypeDescriptor> destTypes = ((StaticConduitTypeDescriptor)targetTypeDesc).destTypes;
							List<SymbolEntry<Type, Node, TId>> possibleEntries = new LinkedList<SymbolEntry<Type, Node, TId>>();
							for (ClassOrContractTypeDescriptor destType : destTypes) {
								searchScopes.add(destType.nameScope);
							}
						}

					} else {
						addLinePosExc(id, "Type is not a conduit, object, or contract");
						continue;
					}

					/* Check semantic rules, regarding method binding, and eliminate those method candidates
						that are not allowed:
						- A method may not be defined twice at different levels of scope nesting: see,
							"Method Redefinition in Nested Name Scopes".
						....TBD
						*/

				} else { // method name is unqualified: search scopes are the current context
					searchScopes.add(methodCallRef.scopeOfUse);
				}
			}

			/* Find all of the potentially matching methods within the search scopes, and from those
				matches assemble a v-table: */
			List<VTableEntry> vTableEntries = vTableFactory.createVTable(methodName, searchScopes,
				methodCallRef.argSection, methodCallRef.assignValueDef,
				false // don't search enclosing scopes
				);

			if (vTableEntries.size() == 0) {
				addLinePosExc(id, "No matching method found");
				continue next_method_call;
			}

			// Annotate the method_call node:
			this.state.setVTableEntries(methodCallRef.methodCall, vTableEntries);

			// All the return types in the v-table should be the same, so just pick the first one:
			FlowTypeDescriptor returnTypeDesc = vTableEntries.get(0).returnType;

			/* Set annotations: */
			this.state.setVTableEntries(methodCallRef.methodCall,
				vTableEntries.toArray(new VTableEntry[vTableEntries.size()]));
			this.state.setRef(id, classEntry);
			this.state.setTypeDesc(methodCallRef.methodCall, returnTypeDesc);
		}


		/* Conduit mappings: ....
			Analyze type compatibility of mappings.
			*/



		/* Check compatibility of method calls with their targets....
			*/



		/* By now, all declarations have been processed, and all value types have
			been determined. Perform name resolution
			on all qualified names. This annotates each Id of each qualified name
			with the symbol that defines that Id. */

		for (QualNameRef qualNameRef : qualNameRefs) {  // see enterQual_name
			List<TerminalNode> ids = qualNameRef.qualName.Tident();
			SymbolEntry entry =
				nameResolver.resolveSymbol(parseTreeUtils.asTIds(ids), qualNameRef.scopeOfUse,
					this.visibilityChecker);
		}



		/* ....Enforce semantic rules */




		nameResolver.popNameScope();

		// ....Empty the lists of nodes that were revisited, so that if we invoke the
		// analyzer again, these will not be revisited redundantly.
		this.qualNameRefs.clear();
		this.valueDefRefs.clear();
		this.valueTypeSpecRefs.clear();
		this.valueTypeSpecScopes.clear();
		this.values.clear();
		this.exprs.clear();
		//this.funcCallRefs.clear();
		this.methodCallRefs.clear();
	}


	/**
	 Perform implicit value definition identification. See LRM, "Implicit Value Definition".
	 */
	private DeclaredEntry<Type, Node, TId> resolveImplicitly(Value_defContext valueDef) {

		if (couldServeAsDeclaration(valueDef)) { // Treat as a declaration

			System.err.println("At line " + 											// debug
				valueDef.value_decl().Tident().getSymbol().getLine() +  	// debug
				", treating " +  														// debug
				valueDef.value_decl().Tident().getSymbol().getText() + 	// debug
				" as a declaration...");  												// debug

			// Create a new Declared Entry
			DeclaredEntry newEntry = null;
			try {
				System.err.println(">>>Creating decl for " +
					getText(valueDef.value_decl().Tident()));
				newEntry = nameResolver.createDeclaration(parseTreeUtils.asTId(getValueDefId(valueDef), false),
					parseTreeUtils.asNode(valueDef));
			} catch (Exception ex) {
				addLinePosExc(valueDef, ex);
				return null;
			}
			// Perform type resolution: attach all of the types that the symbol 'is'.
			Value_type_specContext typeSpec = valueDef.value_decl().value_type_spec();
			assertThat(typeSpec != null, "Unexpected: value_type_spec not found");
			return newEntry;

		} else {
			return null;
		}
	}

	/**
	 Return the name (Tident) of the specified value_def.
	 */
	TerminalNode getValueDefId(Value_defContext ctx) {
		Value_declContext valueDecl = ctx.value_decl();
		return valueDecl.Tident();
	}

	String getValueDefName(Value_defContext ctx) {
		TerminalNode id = getValueDefId(ctx);
		return getSymbolWrapper().getText(parseTreeUtils.asTId(id));
	}

	/** Ref. LRM, "Value Type Inference".
		Determination is local: does not check context (whether the name is
			declared elsewhere, etc.)
		See also enterValue_def and exitNamespace_scope.
	*/
	private boolean couldServeAsDeclaration(Value_defContext ctx) {
		// If the value_def is declared as a value or constant,
		if ((ctx.Tvalue() != null) || (ctx.Tconstant() != null)) {
			// then it must be a declaration;
			return true;
		// else, if a type (value_type_spec) is specified,
		} else if (ctx.value_decl().value_type_spec() != null) {
			// then it is a declaration
			return true;
		// else, if the type is determinable from the expr,
		} else {
			ParserRuleContext parent = ctx.getParent();
			if (parent instanceof ExprContext) {
				// then it could serve as a declaration;
				ExprContext expr = (ExprContext)parent;
				// Either a left assignment or a right assignment is possible.
				if (expr.left_assign_op() != null) return true;
				if (expr.right_assign_op() != null) return true;
			}
		}

		return false;
	}


	@Override public void enterQual_name(Qual_nameContext ctx) {

		// Add qual_name to list of qual_names to be resolved in exitNamespace_scope.
		this.qualNameRefs.add(new QualNameRef(ctx, nameResolver.getCurrentNameScope()));
	}

	@Override public void enterImport_stmt(Import_stmtContext ctx) {
		List<TerminalNode> qualNameParts = ctx.qual_name().Tident();
		List<Slash_nameContext> slashNames = ctx.slash_name();

		String qualName = nameResolver.createNameFromPath(parseTreeUtils.asTIds(qualNameParts));
		String internalName = "";
		for (Slash_nameContext slashName : slashNames) {
			String namePart = getText(slashName.Tident());
			internalName = internalName + '/' + namePart;
		}

		try {
			getImportHandler().processNamespace(qualName, internalName, false);
		} catch (Exception ex) {
			throw new RuntimeException(ex);
		}
	}

	@Override public void enterFunction_def(Function_defContext ctx) {
		nameResolver.pushScopeName(parseTreeUtils.asTId(ctx.Tident()), parseTreeUtils.asNode(ctx));  // popped in enterFunction_def_scope
	}

	@Override public void enterFunction_def_scope(Function_def_scopeContext ctx) {
		NameScope<Type, Node, TId> newScope = nameResolver.createNameScope();
	}

	@Override public void exitFunction_def_scope(Function_def_scopeContext ctx) {
		nameResolver.popNameScope();
	}

	@Override public void enterValue_def(Value_defContext ctx) {

		Value_declContext valueDecl = ctx.value_decl();
		TerminalNode tnode = valueDecl.Tident();
		if (tnode == null) tnode = ctx.value_decl().Tident();
		String name = getText(tnode);

		if (isAStructDefinition(ctx)) {
			nameResolver.pushScopeName(parseTreeUtils.asTId(tnode), parseTreeUtils.asNode(ctx));
			// A NameScope<Type, Node, TId> will be created when the struct scope is processed,
			// in enterValue_type_spec_scope.
		} else {

			/* From grammar (see value_def production):
				If the value_def is declared as a value or constant,
				   then it must be a declaration;
			*/

			DeclaredEntry entry = null;
			if ((ctx.Tvalue() != null) || (ctx.Tconstant() != null)) {
				// has 'value' or 'constant' keyword - must be a declaration
				try { entry = nameResolver.createDeclaration(asTId(tnode), asNode(ctx), false); }
				catch (Exception ex) {
					addLinePosExc(tnode, ex);
					return;
				}
				// Perform type resolution: attach all of the types that the symbol 'is'.
				//Value_type_specContext typeSpec = ctx.value_decl().value_type_spec();
				//if (typeSpec != null) {
					//analyzeTypeSpec(entry, typeSpec);
				//}

			} else if (ctx.value_decl().value_type_spec() != null) {
				// it specifies a type, so it must be a declaration
				SymbolEntry e = nameResolver.identifySymbolLocally(parseTreeUtils.asTId(tnode));
				Node defNode = null;
				if (e instanceof DeclaredEntry) defNode = ((DeclaredEntry<Node>)e).getDefiningNode();
				if (e != null) { // it has been declared above this in this region,
					addLinePosExc(ctx, name + " has already been declared" +
						(defNode == null? "" : " at line " + getLine(parseTreeUtils.asParseTree(defNode))));
					return;
				} else {
					try { entry = nameResolver.createDeclaration(parseTreeUtils.asTId(tnode), parseTreeUtils.asNode(ctx), false); }
					catch (Exception ex) {
						addLinePosExc(tnode, ex);
						return;
					}
					// Perform type resolution: attach all of the types that the symbol 'is'.
					//analyzeTypeSpec(entry, ctx.value_decl().value_type_spec());
				}

			} else {
				// Add it to a list of possible declarations.
				System.err.println("Adding " + tnode.getText() + " to valueDefRefs"); // debug
				this.valueDefRefs.add(new ValueDefRef(ctx, getCurrentNameScope()));
			}
		}
	}

	@Override public void enterType_def(Type_defContext ctx) {

		NameScope<Type, Node, TId> enclosingScope = getCurrentNameScope();

		Type_declContext typeDecl = ctx.type_decl();
		TerminalNode tnode = typeDecl.Tident();

		if (isAStructDefinition(ctx)) {
			// Save the name of the scope that this defines.
			nameResolver.pushScopeName(parseTreeUtils.asTId(tnode), parseTreeUtils.asNode(ctx));
			// Gets used in enterValue_type_spec_scope.
		} else {
			String name = getText(tnode);
			DeclaredEntry entry = new DeclaredEntry(name, enclosingScope, ctx);
			try { enclosingScope.addEntry(getLine(ctx), name, entry, false); }
			catch (Exception ex) {
				addLinePosExc(tnode, ex);
				return;
			}
		}
	}

	@Override public void enterValue_type_spec(Value_type_specContext ctx) {

		DeclaredEntry<Type, Node, TId> entry = null; // may remain null

		/* Note: the def might not have been set yet, for variables that are implicitly defined.
			As those are recognized, need to update their ValueTypeSpecRef annotations to specify
			the DeclaredEntry field.
			*/
		Node parent = ctx.getParent();
		if (parent instanceof Value_declContext) {
			Value_defContext valueDef = (value_defContext)(parent.getParent());
			entry = this.state.getDef(valueDef);
		} else if (parent instanceof Type_declContext) {
			Type_defContext typeDef = (Type_defContext)(parent.getParent());
			entry = this.state.getDef(typeDef);
		}

		this.valueTypeSpecRefs.add(new ValueTypeSpecRef(getCurrentNameScope(), entry, ctx));
	}

	@Override public void enterValue_type_spec_scope(Struct_type_spec_scopeContext ctx) {

		NameScope<Type, Node, TId> newScope = nameResolver.createNameScope();  // calls popScopeName
		Qual_nameContext qualName = getStructQualName(ctx);
		System.err.println("For value_type_spec_scope, qualName=" +   // debug
			(qualName == null? "null" : nameResolver.getSymbolWrapper().getText(parseTreeUtils.asNode(qualName))));  // debug
		if (qualName != null) {
			// preceded by a qual_name - extends an existing struct or struct type
			// Append the new scope to that one.
			valueTypeSpecScopes.add(new ValueTypeSpecScopeRef(ctx, qualName, newScope, getCurrentNameScope()));
		}
	}

	/*
	@Override public void enterFunction_def(Function_defContext ctx) {
		Value_type_specContext valueTypeSpec = ctx.function_spec().value_type_spec();


	}
	*/

	@Override public void enterArray_literal(Array_literalContext ctx) {
		this.arrayLiteralStack.add(ctx);
		this.arrayLiteralNoOfDims++;
	}

	@Override public void exitArray_literal(Array_literalContext ctx) {

		if (this.arrayLiteralStack.poll() != ctx) {
			throwLinePosExc(ctx, "Unexpected: did not find array_literal on array literal stack");
		}

		if (this.arrayLiteralStack.size() == 0) { // at the outermost array level
			addOutermostArrayLiteral(ctx, this.arrayLiteralNoOfDims);
			this.arrayLiteralNoOfDims = 0; // reset
		}
	}

	@Override public void enterEnum_spec_scope(Enum_spec_scopeContext ctx) {
		nameResolver.createNameScope();  // calls popScopeName
	}

	@Override public void exitEnum_spec_scope(Enum_spec_scopeContext ctx) {
		nameResolver.popNameScope();
	}

	@Override public void enterEnum_value_def(Enum_value_defContext ctx) {

		TId id = parseTreeUtils.asTId(ctx.Tident());
		DeclaredEntry entry = new DeclaredEntry(
			(parseTreeUtils.asTerminalNode(id)).getSymbol().getText(), getCurrentNameScope(), ctx);
		try {
			nameResolver.addSymbolEntry(entry, id, false);
		} catch (SymbolEntryPresent ex) {
			throw new RuntimeException(ex);
		}
	}

	/**
	 Return the qualified name of the borrowed struct type.
	 May return null (if the value_type_spec does not extend the borrowed type).
	 */
	private Qual_nameContext getStructQualName(Struct_type_spec_scopeContext ctx) {
		ParserRuleContext parent = ctx.getParent();
		assertIsA(parent, Value_type_specContext.class);
		return ((Value_type_specContext)parent).qual_name();
	}

	@Override public void exitValue_type_spec_scope(Struct_type_spec_scopeContext ctx) {
		nameResolver.popNameScope();
	}

	private boolean isAStructDefinition(Value_defContext ctx) {
		Value_declContext valueDecl = ctx.value_decl();
		return isAStructDefinition(valueDecl);
	}

	private boolean isAStructDefinition(Type_defContext ctx) {
		Type_declContext typeDecl = ctx.type_decl();
		return isAStructDefinition(typeDecl);
	}

	private boolean isAStructDefinition(Value_declContext valueDecl) {
		Value_type_specContext valueTypeSpec = valueDecl.value_type_spec();
		if (valueTypeSpec == null) return false;
		return isAStructDefinition(valueTypeSpec);
	}

	private boolean isAStructDefinition(Type_declContext typeDecl) {
		Value_type_specContext valueTypeSpec = typeDecl.value_type_spec();
		return isAStructDefinition(valueTypeSpec);
	}

	private boolean isAStructDefinition(Value_type_specContext valueTypeSpec) {
		return valueTypeSpec.value_type_spec_scope() != null;
	}

	@Override public void enterValue(ValueContext ctx) {
		this.values.add(ctx);
	}

	@Override public void enterArray_elt_type_spec(Array_elt_type_specContext ctx) {
		ParserRuleContext parent = ctx.getParent();
		assertIsA(parent, Value_type_specContext.class);
		Value_type_specContext valueTypeSpec = (Value_type_specContext)parent;
		List<Array_def_specContext> arrayDefSpecs = valueTypeSpec.array_def_spec();
		assertThat(arrayDefSpecs.size() > 0, "Unpexpected production: there are no array_def_spec nodes");
		arrayDeclRefs.add(new ArrayEltTypeSpecRef(ctx, arrayDefSpecs));
	}

	@Override public void exitInitialization(InitializationContext ctx) {

		// initialization : Tl_ar expr Tsemicolon ;

		Value_defContext valueDef = ctx.parent().value_def();
		Value_declContext valueDecl = valueDef.value_decl();
		Value_type_specContext arrayValueTypeSpec = valueDecl.value_type_spec(); // might be null
		if (arrayValueTypeSpec != null) {
			ExprContext initEexpr = ctx.expr();
			this.arrayValueTypeSpecInitExprs.put(arrayValueTypeSpec, initEexpr); // the array value init expr
		}
	}

	@Override public void enterInline_func(Inline_funcContext ctx) {
		// Create an anonymous scope for the formal args:
		nameResolver.pushScopeName(null, parseTreeUtils.asNode(ctx));  // popped in enterFunction_def_scope
	}

	@Override public void enterClass_def(Class_defContext ctx) {
		nameResolver.pushScopeName(parseTreeUtils.asTId(ctx.Tident()), parseTreeUtils.asNode(ctx));
	}

	@Override public void enterClass_scope(Class_scopeContext ctx) {
		nameResolver.createNameScope();
	}

	@Override public void exitClass_scope(Class_scopeContext ctx) {
		nameResolver.popNameScope();
	}

	@Override public void enterContract_def(Contract_defContext ctx) {
		nameResolver.createNameScope(parseTreeUtils.asTId(ctx.Tident()), parseTreeUtils.asNode(ctx));
	}

	@Override public void enterContract_def_scope(Contract_def_scopeContext ctx) {
		nameResolver.createNameScope();
	}

	@Override public void exitContract_def_scope(Contract_def_scopeContext ctx) {
		nameResolver.popNameScope();
	}

	@Override public void enterConstructor_def(Constructor_defContext ctx) {
		nameResolver.pushScopeName(parseTreeUtils.asTId(ctx.Tident()), parseTreeUtils.asNode(ctx));
	}

	@Override public void enterConstructor_def_scope(Constructor_def_scopeContext ctx) {
		nameResolver.createNameScope();
	}

	@Override public void exitConstructor_def_scope(Constructor_def_scopeContext ctx) {
		nameResolver.popNameScope();
	}

	@Override public void enterConduit_spec(Conduit_specContext ctx) {

		TId id = parseTreeUtils.asTId(ctx.Tident());
		if (id != null) {
			DeclaredEntry entry = new DeclaredEntry(
				(parseTreeUtils.asTerminalNode(id)).getSymbol().getText(), getCurrentNameScope(), ctx);
			try {
				nameResolver.addSymbolEntry(entry, id, false);
			} catch (SymbolEntryPresent ex) {
				throw new RuntimeException(ex);
			}
			//nameResolver.resolveForwardReferences(entry);
		}
	}

	@Override public void enterMethod_signature(Method_signatureContext ctx) {
		nameResolver.pushScopeName(parseTreeUtils.asTId(ctx.Tident()), parseTreeUtils.asNode(ctx));
	}

	@Override public void enterMethod_signature_scope(Method_signature_scopeContext ctx) {
		nameResolver.createNameScope();
	}

	@Override public void exitMethod_signature_scope(Method_signature_scopeContext ctx) {
		nameResolver.popNameScope();
	}

	@Override public void enterObject_create_spec(Object_create_specContext ctx) {

		TId id = parseTreeUtils.asTId(ctx.Tident());
		DeclaredEntry entry = new DeclaredEntry(
			(parseTreeUtils.asTerminalNode(id)).getSymbol().getText(), getCurrentNameScope(), ctx);
		try {
			nameResolver.addSymbolEntry(entry, id, false);
		} catch (SymbolEntryPresent ex) {
			throw new RuntimeException(ex);
		}
		//nameResolver.resolveForwardReferences(entry);
	}

	@Override public void enterBlock(BlockContext ctx) {
		nameResolver.createNameScope(parseTreeUtils.asNode(ctx));
	}

	@Override public void exitBlock(BlockContext ctx) {
		nameResolver.popNameScope();
	}

	@Override public void enterCatch_clause_scope(Catch_clause_scopeContext ctx) {

		nameResolver.createNameScope(parseTreeUtils.asNode(ctx));

		TId id = parseTreeUtils.asTId(ctx.Tident());
		if (id != null) {
			DeclaredEntry entry = new DeclaredEntry(
				(parseTreeUtils.asTerminalNode(id)).getSymbol().getText(), getCurrentNameScope(), ctx);
			try {
				nameResolver.addSymbolEntry(entry, id, false);
			} catch (SymbolEntryPresent ex) {
				throw new RuntimeException(ex);
			}
			//nameResolver.resolveForwardReferences(entry);
		}
	}

	@Override public void exitCatch_clause_scope(Catch_clause_scopeContext ctx) {
		nameResolver.popNameScope();
	}


	@Override public void enterMethod_call_stmt(Method_call_stmtContext ctx) {
		/*
				method_call_stmt		: sync_method_call
										| async_method_call
										;

				sync_method_call		: value_def left_assign_op method_call Twait Tsemicolon
										| method_call right_assign_op value_def Twait Tsemicolon
										| method_call Twait Tsemicolon
										;

				// NameScope
				async_method_call		: value_def left_assign_op method_call block
										| method_call right_assign_op value_def block
										| method_call block
										;

				method_call				: meth_call_action arg_section ;

				meth_call_action		: ( qual_name Tperiod )? Tident // a method call
										| supports_claim Tperiod Tident // for conduits
										;

			*/

		Method_callContext methodCall = null;
		Value_defContext assignValueDef = null;
		if (ctx.sync_method_call() != null) {
			methodCall = ctx.sync_method_call().method_call();
			assignValueDef = ctx.sync_method_call().value_def();
		} else if (ctx.async_method_call() != null) {
			methodCall = ctx.async_method_call().method_call();
			assignValueDef = ctx.async_method_call().value_def();
		} else
			throwLinePosExc(ctx, "Unexpected production");


		Method_call_actionContext callAction = methodCall.meth_call_action();

		TerminalNode id = callAction.Tident();
		Arg_sectionContext argSection = methodCall.arg_section();

		Supports_claimContext supportsClaim = callAction.supports_claim();
		Qual_nameContext targetQualName = callAction.qual_name();
		NameScope<Type, Node, TId> scopeOfUse = this.nameResolver.getCurrentNameScope();

		assertThat(! (supportsClaim != null && qualName != null));

		MethodCallRef ref = null;
		if (supportsClaim != null) ref = new MethodCallRef(ctx, methodCall, assignValueDef, id,
			argSection, supportsClaim, scopeOfUse);
		if (qualName != null) ref = new MethodCallRef(ctx, methodCall, assignValueDef, targetQualName, id,
			argSection, scopeOfUse);
		assertThat(ref != null);

		this.methodCallRefs.add(ref);
	}

	@Override public void enterFor_stmt(For_stmtContext ctx) {
		// Declare the label, if any.
		TId labelId = parseTreeUtils.asTId(ctx.Tident());
		if (labelId != null) {
			DeclaredEntry entry = new DeclaredEntry(
				(parseTreeUtils.asTerminalNode(labelId)).getSymbol().getText(), getCurrentNameScope(), ctx);
			try {
				nameResolver.addSymbolEntry(entry, labelId, false);
			} catch (SymbolEntryPresent ex) {
				throw new RuntimeException(ex);
			}
			//nameResolver.resolveForwardReferences(entry);
		}

		// Declare the iteration variable:
		For_conditionContext forCond = ctx.for_condition();
		TerminalNode forCondId = forCond.Tident();
		if (forCondId != null) {
			DeclaredEntry entry = new DeclaredEntry(
				forCondId.getSymbol().getText(), getCurrentNameScope(), ctx);
			try {
				nameResolver.addSymbolEntry(entry, parseTreeUtils.asTId(forCondId), false);
			} catch (SymbolEntryPresent ex) {
				throw new RuntimeException(ex);
			}
			//nameResolver.resolveForwardReferences(entry);
		}
	}

	@Override public void enterExpr(ExprContext ctx) {
		exprs.add(ctx);
	}
}
