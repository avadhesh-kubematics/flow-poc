package cliffberg.flow.analyzer;

import cliffberg.symboltable.NameScope;

import java.util.List;
import java.util.LinkedList;

public class NameScopeUtils<Type, Node, TId> {

	/**
	 If the specified type desc is a composite of other type descs, assemble a list of
	 name scopes that represent all of the types.

	 Must include scopes that extend the specified type.

	 Otherwise, return the type spec's name scope.
	 */
	List<NameScope<Type, Node, TId>> assembleNameScopeFromTypes(FlowTypeDescriptor typeDesc,
		boolean includeAppendedScopes) {

		List<NameScope<Type, Node, TId>> nameScopes = new LinkedList<NameScope<Type, Node, TId>>();
		assembleNameScopeFromTypes(typeDesc, nameScopes, includeAppendedScopes);
		return nameScopes;
	}

	void assembleNameScopeFromTypes(FlowTypeDescriptor typeDesc,
		List<NameScope<Type, Node, TId>> nameScopes, boolean includeAppendedScopes) {

		if (typeDesc instanceof CompositeClassOrContractTypeDescriptor) {

			for (ClassOrContractTypeDescriptor tp :
				((CompositeClassOrContractTypeDescriptor)typeDesc).composedOfTypes) {

				assembleNameScopeFromTypes(tp, nameScopes, includeAppendedScopes);
			}

		} else if (typeDesc instanceof ClassOrContractTypeDescriptor) {

			// Add the class's or contract's name scope to the set being assembled:
			nameScopes.nameScopes.add(((ClassOrContractTypeDescriptor)typeDesc).nameScope);

		} else {
			throw new RuntimeException("Expected a ClassOrContractTypeDescriptor");
		}
	}
}
