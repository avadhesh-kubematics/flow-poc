package cliffberg.flow.analyzer;

import cliffberg.flow.parser.FlowParser.Method_callContext;
import cliffberg.flow.parser.FlowParser.Function_callContext;
import cliffberg.flow.parser.FlowParser.ExprContext;

import cliffberg.flow.parser.FlowParser.Array_literalContext;

import cliffberg.symboltable.CompilerState;
import cliffberg.symboltable.Annotation;
import cliffberg.symboltable.TypeDescriptor;

import org.antlr.v4.runtime.tree.ParseTree;
import org.antlr.v4.runtime.tree.TerminalNode;

import java.util.Map;
import java.util.HashMap;
import java.util.List;
import java.util.LinkedList;

public class FlowCompilerState extends CompilerState<FlowValueType, ParseTree, ParseTree, TerminalNode> {

	public FlowCompilerState(int maxNoOfErrors) {
		super(maxNoOfErrors, new FlowSymbolWrapper<ParseTree>());
	}

	public void setArrayLiteralAnnot(ParseTree node, ArrayLiteralAnnotation annot) {
		arrayLiteralAnnots.put(node, annot);
	}

	public ArrayLiteralAnnotation getArrayLiteralAnnot(ParseTree node) {
		return arrayLiteralAnnots.get(node);
	}

	public void setTypeConvAnnot(ParseTree node, TypeConverstionAnnotation annot) {
		typeConvAnnots.put(node, annot);
	}

	public TypeConverstionAnnotation getTypeConvAnnot(ParseTree node) {
		return typeConvAnnots.get(node);
	}

	/**

	 */
	public class TypeCheckAnnoation implements Annotation {
		ParseTree node;
		TypeDescriptor claimedTypeDesc;
		TypeCheckAnnoation(ParseTree node, TypeDescriptor claimedTypeDesc) {
			this.node = node; this.claimedTypeDesc = claimedTypeDesc;
		}
	}

	/**

	 */
	public class AutoConvertAnnotation implements Annotation {
		ExprContext expr; FlowDataType fromType; FlowDataType toType;
		AutoConvertAnnotation(ExprContext expr, FlowDataType fromType, FlowDataType toType) {
			this.expr = expr; this.fromType = fromType; this.toType = toType;
		}
	}

	public class CallBindingAnnotation implements Annotation {
		VTableEntry[] entries;
		CallBindingAnnotation(VTableEntry[] entries) {
			this.entries = entries;
		}
	}

	public void setVTableEntries(Method_callContext methodCall, VTableEntry[] entries) {
		setVTableEntriesForNode(methodCall, entries);
	}

	public void setVTableEntries(Function_callContext functionCall, VTableEntry[] entries) {
		setVTableEntriesForNode(functionCall, entries);
	}

	private void setVTableEntriesForNode(ParseTree node, VTableEntry[] entries) {
		CallBindingAnnotation annot = new CallBindingAnnotation(entries);
		callBindingAnnotations.put(node, annot);
	}

	public CallBindingAnnotation getVTableEntries(Method_callContext methodCall) {
		return getVTableEntriesForNode(methodCall);
	}

	public CallBindingAnnotation getVTableEntries(Function_callContext functionCall) {
		return getVTableEntriesForNode(functionCall);
	}

	private CallBindingAnnotation getVTableEntriesForNode(ParseTree node) {
		return callBindingAnnotations.get(node);
	}

	/**

	 */
	public List<Array_literalContext> getTypeAmbiguousArrayLiterals() {
		return this.typeAmbiguousArrayLiterals;
	}

	/**

	 */
	public List<TypeCheckAnnoation> getDynamicTypeCheckers() {
		return this.typeCheckAnnotations;
	}

	/**

	 */
	public List<AutoConvertAnnotation> getAutoConversions() {
		return this.autoConvertAnnotations;
	}

	/**
	 These need to be re-visited and their element types set in their type descriptor
	 based on the type specified in their accompanying value definition. If there is no
	 accompanying value definition, it is an error, since the array element type is then undefined.
	 */
	void addTypeAmbiguousArrayLiteral(Array_literalContext arrayLiteral) {
		this.typeAmbiguousArrayLiterals.add(arrayLiteral);
	}

	/**
	 Attribute the specified node with an attribute that indicates that when the node
	 is evaluated at runtime, that its type should be compared with the specified type descriptor,
	 and if not, throw a runtime exception.
	 */
	void addDynamicTypeChecker(ParseTree node, TypeDescriptor claimedTypeDesc) {
		this.typeCheckAnnotations.add(new TypeCheckAnnoation(node, claimedTypeDesc));
	}

	void addAutoTypeConverter(ExprContext expr, FlowDataType fromType, FlowDataType toType) {
		this.autoConvertAnnotations.add(new AutoConvertAnnotation(expr, fromType, toType));
	}

	private Map<ParseTree, ArrayLiteralAnnotation> arrayLiteralAnnots = new HashMap<ParseTree, ArrayLiteralAnnotation>();

	private Map<ParseTree, TypeConverstionAnnotation> typeConvAnnots = new HashMap<ParseTree, TypeConverstionAnnotation>();

	private List<Array_literalContext> typeAmbiguousArrayLiterals = new LinkedList<Array_literalContext>();

	private List<TypeCheckAnnoation> typeCheckAnnotations = new LinkedList<TypeCheckAnnoation>();

	private List<AutoConvertAnnotation> autoConvertAnnotations = new LinkedList<AutoConvertAnnotation>();

	private Map<ParseTree, CallBindingAnnotation> callBindingAnnotations = new HashMap<ParseTree, CallBindingAnnotation>();
}
