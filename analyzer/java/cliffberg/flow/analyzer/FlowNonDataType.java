package cliffberg.flow.analyzer;

import cliffberg.symboltable.ConversionException;

public enum FlowNonDataType implements FlowValueType {
	FlowConduit, FlowClassOrContract, FlowObject;

	public void checkNativeTypeAssignabilityFrom(Class sourceNativeType) throws ConversionException {
		throw new RuntimeException("Not used so not implemented");
	}
}
