package cliffberg.flow.analyzer;

import static cliffberg.flow.parser.FlowParser.ExprContext;

import cliffberg.symboltable.ExprAnnotation;
import cliffberg.symboltable.TypeDescriptor;

import org.antlr.v4.runtime.ParserRuleContext;

public class SliceAnnotation extends ExprAnnotation<FlowDataType, ParserRuleContext> {

	public ExprContext fromExpr;
	public ExprContext toExpr;

	public SliceAnnotation(ParserRuleContext node, Object valueToBeSliced, TypeDescriptor<FlowDataType> sliceTypeDesc,
		ExprContext fromExpr, ExprContext toExpr) {
		super(node, valueToBeSliced, sliceTypeDesc);
		this.fromExpr = fromExpr;
		this.toExpr = toExpr;
	}
}
