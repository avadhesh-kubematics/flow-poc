// Antlr (https://www.antlr.org/) grammar for version 1 of Flow.
// Copyright (c) 2018 by Cliff Berg.
// Usage is subject to Creative Commons Attribution-NoDerivatives 4.0 International (CC BY-ND 4.0)
// https://creativecommons.org/licenses/by-nd/4.0/
// The no-derivatives license is used in order to ensure language integrity
// and compatibility between compiler implementations.
// Derivatives will be considered with interest, but require permission.

grammar Flow;

	// ---------------------------------------------------------------------------
	// Tokens --------------------------------------------------------------------

	// Special symbols:
	Tlr_dbl_ar				: '<-->' ;
	Tl_dbl_ar				: '<--' ;
	Tr_dbl_ar				: '-->' ;
	Tgtplus					: '>+' ;
	Tpluslt					: '+<' ;
	Tgtminus				: '>-' ;
	Tminuslt				: '-<' ;
	Tinc					: '++' ;
	Tdec					: '--' ;
	Tl_ar					: '<-' ;
	Tr_ar					: '->' ;
	Tdotdot					: '..' ;
	Tge						: '>=' ;
	Tle						: '<=' ;
	Tne						: '!=' ;
	Tbiteq					: '==' ;
	Tbitne					: '-=' ;
	Tbitand					: '/\\' ;
	Tbitor					: '\\/' ;
	Tbitxor					: '\\|/' ;
	Tlshift					: '<<' ;
	Trshift					: '>>' ;
	Tcolon					: ':' ;
	Tsemicolon				: ';' ;
	Tperiod					: '.' ;
	Tcomma					: ',' ;
	Tasterisk				: '*' ;
	Tslash					: '/' ;
	Teq						: '=' ;
	Tgt						: '>' ;
	Tlt						: '<' ;
	Tplus					: '+' ;
	Tminus					: '-' ;
	Tl_curl_br				: '{' ;
	Tr_curl_br				: '}' ;
	Tl_paren				: '(' ;
	Tr_paren				: ')' ;
	Tl_sqbr					: '[' ;
	Tr_sqbr					: ']' ;
	Tcaret					: '^' ;
	Tapos					: '\'' ;
	Tquote					: '"' ;

	// Keywords:
	Tabstract				: 'abstract' ;
	Tactive					: 'active' ;
	Tand					: 'and' ;
	Tapply					: 'apply' ;
	Tboolean				: 'boolean' ;
	Tbreak					: 'break' ;
	Tbyte					: 'byte' ;
	Tby						: 'by' ;
	Tcase					: 'case' ;
	Tcatch					: 'catch' ;
	Tclass					: 'class' ;
	Tcleanup				: 'cleanup' ;
	Tconcurrent				: 'concurrent' ;
	Tconduit				: 'conduit' ;
	Tconstant				: 'constant' ;
	Tcontinue				: 'continue' ;
	Tcontract				: 'contract' ;
	Tcreate					: 'create' ;
	Tdestroy				: 'destroy' ;
	Tdisable				: 'disable' ;
	Telse					: 'else' ;
	Tenable					: 'enable' ;
	Tenum					: 'enum' ;
	Tever					: 'ever' ;
	Tfalse					: 'false' ;
	Tfinally				: 'finally' ;
	Tfloat					: 'float' ;
	Tfor					: 'for' ;
	Tfunction				: 'function' ;
	Tglyph					: 'glyph' ;
	Tif						: 'if' ;
	Timport					: 'import' ;
	Tint					: 'int' ;
	Tin						: 'in' ;
	Tis						: 'is' ;
	Tlong					: 'long' ;
	Tmethod					: 'method' ;
	Tnamespace				: 'namespace' ;
	Tnative					: 'native' ;
	Tnot					: 'not' ;
	Tor						: 'or' ;
	Totherwise				: 'otherwise' ;
	Tprivate				: 'private' ;
	Treplaceable			: 'replaceable' ;
	Treturn					: 'return' ;
	Tsequential				: 'sequential' ;
	Tstring					: 'string' ;
	Tsupports				: 'supports' ;
	Tto						: 'to' ;
	Ttrue					: 'true' ;
	Ttry					: 'try' ;
	Ttype					: 'type' ;
	Tunsigned				: 'unsigned' ;
	Tvalue					: 'value' ;
	Twait					: 'wait' ;
	Txor					: 'xor' ;


	// Regular expressions:
	fragment
	Twhole_number_literal	: '0' | [1-9][0-9]* ;

	Tfloat_literal			: Tint_literal '.' Twhole_number_literal
								( [eEdD] Tint_literal )? ;
	Tint_literal			: [+\-]? Twhole_number_literal ;

	Thex_chars				: [a-fA-F0-9]+ ;

	Tunicode_spec			: '%' 'u' Tl_paren Thex_chars Tr_paren ;

	Tescaped_char			: [nrt"'\\] ;

	Tescape_seq				: '\\' Tescaped_char ;

	fragment
	Tascii_char				: ' '..'!' | '#'..'$' | '&' | '('..'[' | ']'..'~' ;

	Tascii_char_seq			: Tascii_char+ ;

	Tglyph_literal			: Tapos ( Tascii_char | Tunicode_spec | Tescape_seq ) Tapos ;

	Tstring_element			: Tascii_char_seq | Tunicode_spec | Tescape_seq ;

	Tstring_literal			: Tquote Tstring_element* Tquote ;

	// Comments:
	Tsingle_ln_comment		: '//' ~( '\r' | '\n' )* -> skip ;
	Tmult_ln_comment		: '/*' .*? '*/' -> skip ;
	Tmult_ln_comment_ra		: '/*' .*? '/>' -> skip ;
	Tmult_ln_comment_la		: '</' .*? '*/' -> skip ;

	// Identifier:
	Tident					: [a-zA-Z_] [a-zA-Z_0-9]* ;

	// Whitespace:
	WS 						: [ \t\r\n]+ -> skip ;




	// ---------------------------------------------------------------------------
	// Productions ---------------------------------------------------------------
	//
	// Note: Any production name that is suffixed with "_scope" enbloses a name scope.


	// Top level elements ------------------------------------

	// NameScope - inherits Global
	// DeclaredEntry
	namespace				: namespace_decl namespace_scope ;

	namespace_scope			: import_stmt* namespace_elt* ;

	namespace_decl			: Tnamespace Tident ( Tperiod Tident )* colon_name* Tcolon ;

	colon_name				: Tcolon Tident ;

	import_stmt				: ( Timport | Tapply ) qual_name ;

	per_colon				: Tperiod
							| Tcolon ;

	per_colon_ast			: per_colon Tasterisk ;

	namespace_elt			: value_def initialization?
							| contract_def
							| type_def
							| class_def
							| function_def
							| method_def
							| conduit_spec
							;

	qual_name			: Tident ( ( Tperiod | Tcolon ) Tident )* ;

		qual_name_seq		: qual_name ( Tcomma qual_name )* ;

	// NameScope - inherits current
	// DeclaredEntry
	function_def			: Tfunction Tident function_spec ;

	function_def_scope		: arg_def_seq? ;

	function_spec			: Tl_paren function_def_scope Tr_paren
								value_type_spec   // return type
								throws_spec?
								( block   // certain statement types disallowed in block
									| Tnative Tsemicolon )
							;




	// Values ------------------------------------------------

	// NameScope (if a struct definition -
	// DeclaredEntry
	// Note: A value def may semantically represent a value definition, or it might merely
	// be a reference to an existing value definition. The ambiguity can only be resolved
	// semantically. See LRM, "Implicit Value Definition".
	value_def				: ( Tvalue | Tconstant )? value_decl ;
							// See LRM, "Value Type Inference". Semantic rules:
							// If the value_def is declared as a value or constant,
							//    then it must be a declaration;
							//    else, if a value_type_spec is specified,
							//       then it is a declaration
							//       else, if the name is declared elsewhere and visible
							//             in the current static context),
							//          then the value_def is a reference to that;
							//          else, if it is assigned from an expr, and the type
							//                is determinable from the expr,
							//             then it is considered to be a declaration;
							//             else, it is an error.

	// NameScope (if a struct definition - same rules as for value_def)
	// DeclaredEntry
	type_def				: Ttype type_decl ;

	array_def_spec			: Tl_sqbr expr? Tr_sqbr ;  // if no expr, then the size is dynamic

	// Defines a value and its type. Can be used in a value definition (with a
	// 'value' keyword), or in an inline value definition (no 'value' keyword).
	value_decl				: Tident ( Tcolon value_type_spec )? ;

	type_decl				: Tident Tcolon value_type_spec ;

	value_type_spec			: intrinsic_type_spec
							| qual_name
							| struct_type_spec
							| enum_spec_scope
							| array_elt_type_spec array_def_spec+
							;

	struct_type_spec		: qual_name_seq? struct_type_spec_scope
							;

	struct_type_spec_scope	: struct_init ;

	array_elt_type_spec		: intrinsic_type_spec
							| qual_name
							;

	value					: literal
							| qual_name
							| qual_name_seq struct_init
							| intrinsic_type_conv
							| Tl_paren expr Tr_paren
							| Tl_paren if_expr Tr_paren
							| Tl_paren case_expr Tr_paren
							;

	literal					: intrinsic_literal
							| array_literal
							;

	intrinsic_literal		: Tint_literal
							| Tfloat_literal
							| boolean_literal
							| Tstring_literal
							| Tglyph_literal
							;

	boolean_literal			: Ttrue
							| Tfalse ;

	array_literal			: Tl_sqbr expr_seq? Tr_sqbr ;

	// NameScope (for the formal args) - inherits current, encloses function_spec.arg_def_seq?
	inline_func				: Tfunction function_spec ;



	// Value types -------------------------------------------

	intrinsic_type_spec		: sign_modifier? size_modifier? Tint
							| size_modifier? Tfloat
							| Tbyte
							| Tboolean
							| Tglyph
							| Tstring
							;

	sign_modifier			: Tunsigned ;

	size_modifier			: Tlong ;

	enum_spec_scope			: Tenum Tl_curl_br enum_value_def ( Tcomma enum_value_def )* Tr_curl_br ;

	enum_value_def			: Tident ;

	struct_init				: Tl_curl_br struct_elt_seq? Tr_curl_br ;

	struct_elt				: value_def ( Tl_ar expr )? | function_def ;

		struct_elt_seq		: struct_elt ( Tcomma struct_elt )* ;



	// For function and method calls -------------------------

	arg_section				: Tl_paren act_arg_seq? Tr_paren ;

	act_arg					: expr | inline_func ;

		act_arg_seq			: act_arg ( Tcomma act_arg )* ;



	// Classes -----------------------------------------------

	// NameScope - inherits the classes/contracts specified by is_seq. May reference types that are vislble in the current context.
	// DeclaredEntry
	class_def				: concurrent_spec? Tclass Tident is_seq?
								Tl_curl_br class_scope Tr_curl_br
							;

	class_scope				: class_elt* ;

	// NameScope
	// DeclaredEntry
	contract_def			: Tcontract Tident is_seq?
								Tl_curl_br contract_def_scope Tr_curl_br
							;

	contract_def_scope		: contract_elt* ;

	is_seq					: Tcolon qual_name ( Tcomma qual_name )* ;

	concurrent_spec			: Tconcurrent ( Tl_paren Tint_literal Tr_paren )? ;
							// the int literal must be unsigned

	class_elt				: constructor_def
							| class_def
							| contract_def
							| type_def
							| value_def initialization?
							| function_def
							| method_def
							| conduit_spec
							;

	initialization			: Tl_ar expr Tsemicolon ;

	contract_elt			: method_signature ;

	// NameScope
	// DeclaredEntry
	method_signature		: method_constraint* Tmethod Tident
								Tl_paren method_signature_scope Tr_paren
								method_return_type?
								throws_spec?
							;

	method_signature_scope	: arg_def_seq? ;

	method_constraint		: Tprivate | Tsequential | Tabstract | Treplaceable ;

	method_return_type		: value_type_spec
							| conduit_ret_type_spec
							;

	// NameScope
	// DeclaredEntry
	constructor_def			: Tident
								Tl_curl_br mapping_decl_seq? Tr_curl_br
								Tl_paren constructor_def_scope Tr_paren
								throws_spec?
								( block
									| Tnative Tsemicolon )
							;

	constructor_def_scope	: arg_def_seq? ;

	mapping_decl			: ( ( Tident )? Tr_dbl_ar )? Tident ( binding_op Tident )? ;

	mapping_decl_seq		: mapping_decl ( Tcomma mapping_decl )* ;

	// DeclaredEntry
	arg_def					: value_decl ;

	arg_def_seq				: arg_def ( Tcomma arg_def )* variadic_def? ;

	variadic_def			: arg_def Tdotdot ;

	throws_spec				: Tcaret qual_name_seq ;

	// Define a conduit:
	// DeclaredEntry
	conduit_spec			: Tconduit Tident? elasticity? cleanup_handler? ;  // name can be a class or an object

	elasticity				: Tl_sqbr Tr_sqbr ;

	cleanup_handler			: Tcleanup block ;

	create_stmt				: object_create_spec Tsemicolon ;

	// For connecting objects via conduits:
	conduit_binding_stmt	: conduit_ref binding_op queue_spec? object_or_class_ref Tsemicolon
							| object_or_class_ref queue_spec? binding_op conduit_ref Tsemicolon
							;

	queue_spec				: Tl_sqbr Tint_literal Tr_sqbr ;
							// the int literal must be unsigned

	binding_op				: Tlr_dbl_ar
							| Tl_dbl_ar
							| Tr_dbl_ar
							;

	conduit_ref				: conduit_spec  // declares a conduit inline
							| qual_name // references a conduit that is declared elsewhere
							| method_call_stmt // must be a method that returns a conduit
							;

	object_or_class_ref		: object_create_spec
							| Tident  // object name or class name
							;

	method_def				: method_signature
								( block
									| Tnative Tsemicolon )
							;

	// For method return type declaration (must be private method):
	conduit_ret_type_spec	: Tconduit Tr_dbl_ar qual_name_seq
							//                   classref
							| qual_name_seq Tl_dbl_ar Tconduit ( Tr_dbl_ar qual_name_seq )?
							// classref                                classref
							;

	// DeclaredEntry
	object_create_spec		: Tcreate Tident Tcolon qual_name
								Tl_curl_br conduit_map_seq? Tr_curl_br
								Tl_paren expr_seq? Tr_paren
							//                     classref
							// constructor-name { conduit-spec... } ( args ) ;
							;

	conduit_map				: ( qual_name | tuple ) ( ( Tl_dbl_ar | Tr_dbl_ar | Tlr_dbl_ar ) ( qual_name | tuple )? )? ;

		conduit_map_seq		: conduit_map ( Tcomma conduit_map )* ;

		tuple				: Tl_paren qual_name ( Tcomma qual_name )* Tr_paren ;



	// Statements --------------------------------------------
	// Some statement types are disallowed in some contexts.

	stmt					: control_stmt
							| expr_stmt
							| method_call_stmt
							| Tsequential? block  // Tsequential only allowed in a method
							| try_stmt
							;

	control_stmt			: for_stmt
							| break_stmt
							| continue_stmt
							| return_stmt
							| if_stmt
							| case_stmt
							| throw_stmt
							| en_dis_able_stmt		// disallowed inside a function
							| create_stmt			// " "
							| destroy_stmt			// " "
							| conduit_binding_stmt	// " "
							;

	method_call_stmt		: sync_method_call
							| async_method_call
							;

	sync_method_call		: value_def left_assign_op method_call Twait Tsemicolon
							| method_call right_assign_op value_def Twait Tsemicolon
							| method_call Twait Tsemicolon
							;

	// NameScope
	async_method_call		: value_def left_assign_op method_call block
							| method_call right_assign_op value_def block
							| method_call block
							;

	method_call				: meth_call_action arg_section ;

	meth_call_action		: ( qual_name Tperiod )? Tident
							| supports_claim Tperiod Tident // target must be a conduit or an object
							;

	expr_stmt				: expr Tsemicolon ;

	throw_stmt				: Tcaret qual_name struct_init? Tsemicolon ;

	// NameScope
	block					: Tl_curl_br block_scope Tr_curl_br ;

	block_scope				: block_elt* ;

	block_elt				: stmt
							| function_def
							;

	try_stmt				: Ttry stmt*
								(
								  catch_clause+ finally_clause?
								| finally_clause
								)
							;

	// NameScope (for the exceptions, if any)
	// DeclaredEntry
	catch_clause			: Tcatch Tl_paren catch_clause_scope Tr_paren stmt ;

	catch_clause_scope		: ( Tident Tcolon )? qual_name ;

	finally_clause			: Tfinally stmt ;

	// DeclaredEntry (the label, and the iteration variable)
	for_stmt				: ( Tident Tcolon )? Tfor for_condition block ;

		for_condition		: Tever
							| Tident Tin range ( Tby expr )?
							;

		range				: expr // must be a collection or a type name
							| expr Tto expr // from to
							;

	break_stmt				: Tbreak Tident? Tsemicolon ;

	continue_stmt			: Tcontinue Tident? Tsemicolon ;

	return_stmt				: Treturn expr? Tsemicolon ;

	if_stmt					: Tif expr Tcomma? stmt ( Telse stmt )? ;
							//    expr must be boolean;

	case_stmt				: Tcase expr Tl_curl_br case_stmt_choice*
								( Totherwise stmt )? Tr_curl_br ;

		case_stmt_choice	: expr Tcolon stmt ;

	en_dis_able_stmt		: en_dis_able_seq Tsemicolon ;

		enable_clause		: Tenable ident_seq ;
							//        conduit

		disable_clause		: Tdisable ident_seq ;
							//         conduit

		ident_seq			: Tident ( Tcomma Tident )* ;

		en_dis_able			: enable_clause | disable_clause ;

		en_dis_able_seq		: en_dis_able ( Tcomma en_dis_able )* ;

	destroy_stmt			: Tdestroy Tident ;




	// Expressions -------------------------------------------

	expr					:
							  expr array_spec  // for selecting array elements
							| logic_expr
							| value_type_assertion
							| expr Tperiod Tident // a struct field selection
							| value_def left_assign_op expr
							| expr right_assign_op value_def
							| function_call
							;

		expr_seq			: expr ( Tcomma expr )* ;

	left_assign_op			: Tl_ar | Tpluslt | Tminuslt ;

	right_assign_op			: Tr_ar | Tgtplus | Tgtminus ;

	function_call			: qual_name arg_section ;

	intrinsic_type_conv		: intrinsic_type_spec Tl_paren expr Tr_paren ;

	value_type_assertion	: Tl_paren expr Tis (
								  qual_name // for structs
								| array_elt_type_spec array_def_spec+  // for arrays
								) Tr_paren
							;

	supports_claim			: Tl_paren Tident Tsupports qual_name Tr_paren ; // for conduits and objects

	array_spec				: Tl_sqbr (
								  Tdotdot expr
								| expr Tdotdot
								| expr  // result is a single element - not an array
								| expr Tdotdot expr
								| Tactive  // only allowed for an elastic conduit
								) Tr_sqbr
							;

	logic_expr				: comp_expr ( logic_op logic_expr )? ;

		logic_op			: Tand | Tor | Txor ;

	comp_expr				: Tnot? num_expr ( comp_op comp_expr )? ;

		comp_op				: Teq | Tgt | Tlt | Tge | Tle | Tne ;

	num_expr				: mult_expr ( add_op num_expr )? ;

		add_op				: Tplus | Tminus ;

	mult_expr				: exp_expr ( mult_op mult_expr )? ;

		mult_op				: Tasterisk | Tslash ;

	exp_expr				: unop_expr ( Tcaret exp_expr )? ;

	unop_expr				: value inc_op? ;

		inc_op				: Tinc | Tdec ;


	if_expr					: Tif expr Tcomma? expr ( Telse expr ) ;
							//    first expr must be boolean

	case_expr				: Tcase expr Tl_curl_br case_expr_choice*
								( Totherwise expr )? Tr_curl_br ;

		case_expr_choice	: expr Tcolon expr Tsemicolon ;
