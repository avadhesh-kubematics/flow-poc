package cliffberg.flow.generator;

import cliffberg.flow.parser.FlowBaseListener;
import cliffberg.flow.parser.FlowParser;

import cliffberg.symboltable.Annotation;
import cliffberg.symboltable.CompilerState;
import cliffberg.symboltable.NameScope;

import cliffberg.utilities.AssertionUtilities;

import org.antlr.v4.runtime.*;
import org.antlr.v4.runtime.tree.*;

import net.bytebuddy.ByteBuddy;  // https://bytebuddy.net/javadoc/1.9.2/index.html
import net.bytebuddy.dynamic.DynamicType;

import java.io.File;
import java.io.IOException;
import java.util.Map;
import java.util.HashMap;

public class FlowGenerator extends FlowBaseListener {
	
	private CompilerState state;
	private Map<String, DynamicType> classes = new HashMap<String, DynamicType>();
	
	
	public FlowGenerator(CompilerState state) {
		this.state = state;
	}
	
	
	public Map<String, DynamicType> getClasses() { return classes; }
	
	/**
	 Write all of the generated Java classes to the specified root folder. The
	 required child folders will be created according to the package structure
	 of the classes being written.
	 */
	
	public void write(File javaClassRootFolder) throws IOException {
		for (String classname : classes.keySet()) {
			classes.get(classname).saveIn(javaClassRootFolder);
		}
	}
	
	
	public void enterNamespace(FlowParser.NamespaceContext ctx) {
		System.err.println("FlowGenerator.enterNamespace: ");
		
		// A namespace is a Flow class. Model it at a Java class.
		
		// Obtain the namespace symbol table entry:
		Annotation annot = state.getDef(ctx);
		AssertionUtilities.assertIsA(annot, NameScope.class);
		NameScope nameScope = (NameScope)annot;
		String namespaceFullName = nameScope.getName();  // may include a slash
		System.err.println(namespaceFullName);

		String javaClassName = namespaceFullName.replace('/', '.');
		
		try {
			classes.put(javaClassName,
				new ByteBuddy()
				.subclass(Object.class)
				.name(javaClassName)
				.make());
		} catch (Exception ex) {
			throw new RuntimeException(ex);
		}
	}

	/**
	 
	 */
	public ....function_def generateVirtualFunctionBindingSelector(....function ref annot, ....candidate defs)
	throws
		MultipleBindingsFound,
		NoBindingsFound
	{
		....
	}

	/**
	 
	 */
	public ....method_def generateVirtualMethodBindingSelector(....method ref annot, ....candidate defs)
	throws
		MultipleBindingsFound,
		NoBindingsFound
	{
		....
	}
	
}
