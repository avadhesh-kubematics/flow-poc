# Configurations for makefile.
# Edit this file to specify the paths and versions of required tools and directories.

export os := $(shell uname)

ifeq ($(os),Darwin)
	include env.mac
endif

ifeq ($(os),Linux)
	include env.linux
endif



# Configurations particular to why this build is being run:
export maxerrs = 5
.DELETE_ON_ERROR:
.DEFAULT_GOAL: jars

# Versions produced by this build:
export FLOW_COMPILER_VERSION = 0.1
export FLOW_LANGUAGE_VERSION = 1

# Locations of dev tools:
export MVN_REPO := $(HOME)/.m2/repository
export AntlrDir := $(APPLICATIONS)/Antlr

CUCUMBER_CLASSPATH := $(MVN_REPO)/info/cukes/cucumber-java/1.2.5/cucumber-java-1.2.5.jar
CUCUMBER_CLASSPATH := $(CUCUMBER_CLASSPATH):$(MVN_REPO)/info/cukes/cucumber-core/1.2.5/cucumber-core-1.2.5.jar
CUCUMBER_CLASSPATH := $(CUCUMBER_CLASSPATH):$(MVN_REPO)/info/cukes/cucumber-jvm-deps/1.0.5/cucumber-jvm-deps-1.0.5.jar
CUCUMBER_CLASSPATH := $(CUCUMBER_CLASSPATH):$(MVN_REPO)/info/cukes/gherkin/2.12.2/gherkin-2.12.2.jar
export CUCUMBER_CLASSPATH
