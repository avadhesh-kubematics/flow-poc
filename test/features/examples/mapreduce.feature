Feature: Map-Reduce
	
	@done
	Scenario: Map-Reduce
		Given a map-reduce program
		"""
namespace cliffberg.flow.examples.mapreduce:

method main(args: String[]) {
    for arg in args {
        map(arg);
    }
    reduce();
}

conduit con[] --> Compute;

method map(key: string) {
    create c: Compute(key) <-- con;
}

method reduce() {
    con.go();
}

class Compute {
    Compute{}(key: string) {
        value this.key: string = key;
    }
    
    method go() {
        //...perform computation for data in the space of key
    }
}
		"""
		
		Then the map reduce test is parsed without error
		
		And the map reduce test analyzes without an error
		
		#And the map reduce test generates ....
