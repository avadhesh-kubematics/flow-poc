Feature: Producer-Consumer
	
	@done
	Scenario: Producer Consumer
		Given a producer-consumer program
		"""
namespace cliffberg.flow.examples.producerconsumer:

method main(arg: String[]) {
	create consumer: Consumer{}();
    create producer: Producer{consumer}();
    conduit start --> producer;
start.start();
}

class Producer {
    conduit eventChannel: Consumer;
    Producer{eventChannel}() {}
    method start() {
        create random1: Random{}(123.0);
        create random2: Random{}(789.0);
        for ever {
            // sleep random no of secs:
            sleepMs(truncate(random1.random() * 1000.0));

            // Send data - a random number from 0 to 10000:
            int(random2.random() * 10000.0) -> eventChannel;
        }
    }
}

class Consumer {
    method postEvent(event: int) {
        event -> stdout;
    }
}
		"""
		
		Then the producer consumer test is parsed without error
		
		And the producer consumer test analyzes without an error
		
		#And the producer consumer test generates ....
