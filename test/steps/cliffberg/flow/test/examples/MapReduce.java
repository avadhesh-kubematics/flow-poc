package cliffberg.flow.test.examples;

import cliffberg.flow.test.TestBase;

import org.antlr.v4.runtime.tree.ParseTree;
import org.antlr.v4.runtime.tree.TerminalNode;

import cucumber.api.Format;
import cucumber.api.java.Before;
import cucumber.api.java.After;
import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;

public class MapReduce extends TestBase {

	@Given("^a map-reduce program$")
	public void a_map_reduce_program(String source) throws Throwable {
		setSource(source);
	}
	
	@Then("^the map reduce test is parsed without error$")
	public void the_map_reduce_test_is_parsed_without_error() throws Exception {
		checkParse();
	}
		
	@Then("^the map reduce test analyzes without an error$")
	public void the_map_reduce_test_analyzes_without_an_error() throws Exception {
		checkAnalyze();
	}
	
	/*
	@Then("^the map reduce test generates ....$")
	public void the_map_reduce_test_generates_....() throws Exception {
		checkGenerate();
		....
	}
	*/
}
